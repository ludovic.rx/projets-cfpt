<!--
*    Projet    :   Friend Plan
*    Auteur    :   Ludovic Roux et Dan Bonvallat
*    Desc.     :   Page d'ajout
*    Version   :   1.0, 16.09.2020, LR et DB, version initiale
-->
<?php
include_once("php/functions.inc.php");
include_once("php/session.inc.php");

$nom = "";
$adresse = "";
$heureDebut = "";
$heureFin = "";
$date = "";

$newEvent = array();
$eventsUser = null;

if (validInput($nom, INPUT_POST, 'nomEvenement', FILTER_SANITIZE_STRING, FILTER_DEFAULT) &&
    validInput($adresse, INPUT_POST, 'nomAdresse', FILTER_SANITIZE_STRING, FILTER_DEFAULT) &&
    validInput($heureDebut, INPUT_POST, 'debutHeure', FILTER_SANITIZE_STRING, FILTER_DEFAULT) &&
    validInput($heureFin, INPUT_POST, 'finHeure', FILTER_SANITIZE_STRING, FILTER_DEFAULT) &&
    validInput($date, INPUT_POST, "date", FILTER_SANITIZE_STRING, FILTER_DEFAULT)) {
    $newEvent = array("nom" => $nom, "adresse" => $adresse, "heureDebut" => $heureDebut, "heureFin" => $heureFin, "date" => $date);
    $eventsUser = getEvent($_SESSION["idUser"])["events"];
    if($eventsUser == "") {
        $eventsUser = null;
    }
}

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="style/main.css">
    <link rel="icon" href="assets/icon_friend_plan.PNG"/>

    <title>Sans trajet</title>
</head>

<body>
    <div class="divMain">
        <?php include_once("./php/header.inc.php"); ?>
        <main>
            <form action="#" method="POST" id="form">
                <div class="date" id="date">
                    <h2 id="jour">Mercredi 2 février</h2>
                    <input type="date" class="start" id="choixDate" name="date" value="2020-09-23" min="2000-01-01" max="2099-12-31">
                </div>
                <div class="form-input">
                    <div>
                        <input type="text" id="nameEvent" name="nomEvenement" placeholder="Nom de l'évenement" value="<?php echo $nom; ?>">
                    </div>
                    <div>
                        <label></label>
                        <input type="text" id="adress" name="nomAdresse" value="<?php echo $adresse; ?>" placeholder="Adresse">
                    </div>
                    <div>
                        <label class="lblHeure">Début :</label>
                        <input type="time" id="timeStart" name="debutHeure" min="00:00" max="23:59" value="<?php echo $heureDebut ?>">
                    </div>
                    <div>
                        <label class="lblHeure">Fin :</label>
                        <input type="time" id="timeEnd" name="finHeure" min="00:00" max="23:59" value="<?php echo $heureFin ?>">
                    </div>
                </div>
            </form>
        </main>
        <footer>
            <button class="halfSize" onclick="window.location.href='ajout.php'" id="btnRetour">Retour</button>
            <button class="halfSize" id="btnValider" onclick='document.getElementById("form").submit();'>Valider</button>
        </footer>
    </div>
</body>
<script src="js/calendrier.js"></script>
<script src="js/evenement.js"></script>
<script src="js/cookie.js"></script>
<script>
    var evenement;

    document.getElementById("jour").innerHTML = cal.writeDay();
    document.getElementById("choixDate").value = cal.getFormattedDate();

    document.getElementById("btnRetour").addEventListener("click", function() {
        document.cookie = cal.createCookie()
    });

    document.getElementById("choixDate").addEventListener("change", function() {
        cal.setDay(this.value, cal);
        document.getElementById("jour").innerHTML = cal.writeDay();
    });

    if(<?= $newEvent == array() ? "false" : "true" ?>) {
        let values = <?= json_encode($newEvent) ?>;     
        let events = JSON.parse(<?= json_encode($eventsUser) ?>);
        evenement = new EvenementWhitoutDrive(values.nom, values.heureDebut, values.heureFin, values.adresse, new Date(values.date));
        if(events == null) {
            events = new Array();        
        }
        events.push(evenement);
        document.cookie = "events=" + JSON.stringify(events);
        window.location.href = 'addEvent.php?typeEvent=0';
    }
</script>

</html>