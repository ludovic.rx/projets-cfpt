<!--
*    Projet    :   Friend Plan
*    Auteur    :   Ludovic Roux et Dan Bonvallat
*    Desc.     :   Page d'acceuil
*    Version   :   1.0, 16.09.2020, LR et DB, version initiale
-->
<!DOCTYPE html>
<?php
include_once("php/session.inc.php");
include_once("php/functions.inc.php");

$errorConnection = false;
validInput($errorConnection, INPUT_GET, "errorConnection", FILTER_SANITIZE_NUMBER_INT, FILTER_VALIDATE_BOOLEAN);
    
?>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="style/main.css">
    <link rel="icon" href="assets/icon_friend_plan.PNG"/>

    <title>Accueil</title>
</head>

<body>
    <div class="divMain">
        <?php include_once("./php/header.inc.php"); ?>
        <main>
            <div class="calendar" id="calendar">
                <div class="mois" id="mois">
                    <button type="button" class="changeMonth" id="btnLeft">&#60;</button>
                    <h2>Février 2020</h2>
                    <button type="button" class="changeMonth" id="btnRight">&#62;</button>
                </div>
                <div class="nomJours">
                    <div>Lu</div>
                    <div>Ma</div>
                    <div>Me</div>
                    <div>Je</div>
                    <div>Ve</div>
                    <div>Sa</div>
                    <div>Di</div>
                </div>
                <div class="jours" id="jours">
                    <table>
                        <tr id="line0">
                            <td id="col1"></td>
                            <td id="col2"></td>
                            <td id="col3"></td>
                            <td id="col4"></td>
                            <td id="col5"></td>
                            <td id="col6"></td>
                            <td id="col0"></td>
                        </tr>
                        <tr id="line1">
                            <td id="col1"></td>
                            <td id="col2"></td>
                            <td id="col3"></td>
                            <td id="col4"></td>
                            <td id="col5"></td>
                            <td id="col6"></td>
                            <td id="col0"></td>
                        </tr>
                        <tr id="line2">
                            <td id="col1"></td>
                            <td id="col2"></td>
                            <td id="col3"></td>
                            <td id="col4"></td>
                            <td id="col5"></td>
                            <td id="col6"></td>
                            <td id="col0"></td>
                        </tr>
                        <tr id="line3">
                            <td id="col1"></td>
                            <td id="col2"></td>
                            <td id="col3"></td>
                            <td id="col4"></td>
                            <td id="col5"></td>
                            <td id="col6"></td>
                            <td id="col0"></td>
                        </tr>
                        <tr id="line4">
                            <td id="col1"></td>
                            <td id="col2"></td>
                            <td id="col3"></td>
                            <td id="col4"></td>
                            <td id="col5"></td>
                            <td id="col6"></td>
                            <td id="col0"></td>
                        </tr>
                        <tr id="line5">
                            <td id="col1"></td>
                            <td id="col2"></td>
                            <td id="col3"></td>
                            <td id="col4"></td>
                            <td id="col5"></td>
                            <td id="col6"></td>
                            <td id="col0"></td>
                        </tr>
                    </table>
                </div>
            </div>
        </main>
        <footer>
            <button class="fullSize" id="btnDay" onclick='window.location.href="plan.php"'>Afficher la journée</button>
        </footer>
    </div>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" rel="stylesheet"></script>
<script src="js/calendrier.js"></script>
<script src="js/cookie.js"></script>
<script>
    // Précise où on stocke le calendrier
    cal.divCalendrier = document.getElementById("calendar");

    cal.initButtonMonth(cal, document.getElementById("btnLeft"), document.getElementById("btnRight"));
    cal.afficherMois(cal.getMois(cal.selectedDay), cal);

    document.getElementById("btnDay").addEventListener("click", function() {
        document.cookie = cal.createCookie()
    });

    if(<?= ($errorConnection ? "true" : "false") ?>) {
        alert("Vous devez être connecté pour accéder aux autres pages");  
        window.location.href="index.php";
    }
</script>

</html>