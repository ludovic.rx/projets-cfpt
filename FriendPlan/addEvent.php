<?php
//    Projet    :   Friend Plan
//    Auteur    :   Ludovic Roux et Dan Bonvallat
//    Desc.     :   Ajoute un événement à l'utilisateur 
//    Version   :   1.0, 14.10.2020, LR et DB, version initiale

include_once("php/functions.inc.php");
include_once("php/session.inc.php");
$typeEvent = filter_input(INPUT_GET, "typeEvent", FILTER_SANITIZE_NUMBER_INT);

switch ($typeEvent) {
    case 0: // Evenement sans trajet
    case 1: // Evenement avec trajet
        updateEvent($_SESSION["idUser"], $_COOKIE["events"]);
        header("Location: plan.php");
        break;
    default:
        header("Location: index.php");
        exit();
        break;
}
