//   Projet    :   Friend Plan
//   Auteur    :   Ludovic Roux et Dan Bonvallat
//   Desc.     :   Fichier js qui s'occupe de la page plan
//   Version   :   1.0, 26.12.2020, LR et DB, version initiale

/**
 * Ajoute les événements du jour dans la page plan
 * @param {HTMLElement} table table dans laquelle on affiche les événements
 * @param {array} events tableau des événements de l'utilisateur    
 * @param {Calendrier} cal calendrier qui permet de savoir quelle jour on est
 * @return {void}
 */
function addEvents(table, events, cal) {
    let jsonEvents = Evenement.getListEvents(events);
    let eventsOfToday = Evenement.getEventsOfDay(cal.selectedDay, jsonEvents);

    resetMap();
    writeEvents(table, eventsOfToday, cal);
    addEventsToMap(eventsOfToday);
}

/**
 * Ecrit les événements dans le tableau
 * @param {HTMLElement} table tableau html qui contient le nom des événements
 * @param {array} events tableau des événements de la personne
 * @return {void}
 */
function writeEvents(table, events) {

    let sortedList = Evenement.sortEvents(events);

    table.children[1].innerHTML = "";
    sortedList.forEach(event => {
        let row = `<tr>`;
        row += `<td>${event.timeStart} - ${event.timeEnd}</td>`;
        row += `<td>${event.name}</td>`;
        row += `</tr>`;

        table.children[1].innerHTML += row;
    });
}

/**
 * Permet au bouton droite et gauche de changer les événements affichés sur carte et sur le tableau
 * @param {HTMLElement} btnLeft bouton gauche
 * @param {HTMLElement} btnRight bouton droit
 * @param {HTMLElement} table tableau HTML qui contient les événements
 * @param {array} events tableau des événements de l'utilisateur
 * @param {Calendrier} cal calendrier qui permet de savoir quel est le jour choisi
 * @return {void}
 */
function intiButtonDayPlan(btnLeft, btnRight, table, events, cal) {
    btnLeft.addEventListener("click", function () { addEvents(table, events, cal) });
    btnRight.addEventListener("click", function () { addEvents(table, events, cal) });
}