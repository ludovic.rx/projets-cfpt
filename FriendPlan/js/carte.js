//   Projet    :   Friend Plan
//   Auteur    :   Ludovic Roux et Dan Bonvallat
//   Desc.     :   Fichier qui s'occupe de la map leaflet
//   Version   :   1.0, 04.10.2020, LR et DB, version initiale
var mymap;

/**
 * Cree la map
 * @return {void}
 */
function initializeMap() {
    mymap = L.map('mapid').setView([51.505, -0.09], 13);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1
    }).addTo(mymap);

}

/**
 * Reset la map et enlève tout ce qu'il y avait dessus
 * @return {void}
 */
function resetMap() {
    mymap.remove();
    initializeMap();
}

/**
 * Dessine les différents point de la personne
 * @param {array} values tableau qui contient otus les événements
 * @return {void}
 */
function addEventsToMap(values) {
    let bounds;
    let minBound = null;
    let maxBound = null;

    if (values.length != 0) {
        values.forEach(value => {
            for (let index = 0; index < value.latlngs.length; index++) {
                let latlngs = L.latLng(value.latlngs[index].lat, value.latlngs[index].lng);
                let marker = L.marker(latlngs).addTo(mymap);
                marker.bindPopup(value.name);

                if (minBound == null || latlngs < minBound) {
                    minBound = latlngs;
                }
                if (maxBound == null || latlngs > maxBound) {
                    maxBound = latlngs;
                }

            }
            if(value.latlngs.length == 2) {
                L.polyline([
                    [value.latlngs[0].lat, value.latlngs[0].lng],
                    [value.latlngs[1].lat, value.latlngs[1].lng]
                ]).addTo(mymap)
            }
        });

        bounds = L.latLngBounds(minBound, maxBound);
        mymap.fitBounds([bounds]);
    }
}

initializeMap();