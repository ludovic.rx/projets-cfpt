//   Projet    :   Friend Plan
//   Auteur    :   Ludovic Roux et Dan Bonvallat
//   Desc.     :   Récupère les variables dans les cookies
//   Version   :   1.0, 07.10.2020, LR et DB, version initiale

var cookie = document.cookie;
var cal = new Calendrier();

/**
 * Obtient un cookie grace à son nom
 * @param {string} cname nom du cookie
 * @return {string} le cookie qui a été trouvé
 */
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

if (getCookie("selectedDay") != "") {
    var selectedDay = new Date(getCookie("selectedDay"));
    cal.selectedDay = selectedDay;
} 