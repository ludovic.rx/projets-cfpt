//   Projet    :   Friend Plan
//   Auteur    :   Ludovic Roux et Dan Bonvallat
//   Desc.     :   Class calendrier, qui gère le calendrier de l'index et dans les autres pages
//   Version   :   1.0, 04.10.2020, LR et DB, version initiale
class Calendrier {

    /**
     * Crée un objet calendrier
     */
    constructor() {
        this.divCalendrier = "";
        this.tableBody = "";
        this.affichageMois = "";

        this.mois = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];
        this.tabJours = ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"];
        this.today = new Date(Date.now());
        this.selectedDay = this.today;
    }

    /**
     * Retourne un tableau contenant tous les jours d'un mois
     * @param {Date} date date du jour sélectionné
     * @return {array} tableau de mois
     */
    getMois(date) {
        var tab = [];
        var year = date.getFullYear();
        var month = date.getMonth();

        for (let day = 1; day <= this.getDaysInMonth(month, year); day++) {
            tab.push(new Date(year, month, day));
        }

        return tab;
    }

    /**
     * Affiche le mois dans le tableau HTML
     * @param {Date} tabMois mois choisi
     * @param {Calendrier} calendar Calendrier
     * @return {void}
     */
    afficherMois(tabMois, calendar) {
        this.affichageMois = this.divCalendrier.children[0].children[1];
        this.affichageMois.innerHTML = this.mois[this.selectedDay.getMonth().toString()] + " " + this.selectedDay.getFullYear().toString();
        this.tableBody = this.divCalendrier.children[2].children[0].children[0];
        
        var line = 0;
        var day = 1;

        // Réinitialise les jours du calendrier
        for (let tr = 0; tr < this.tableBody.childElementCount; tr++) {
            this.tableBody.children[tr].classList.add("hidden");
            for (let td = 0; td < this.tableBody.children[tr].childElementCount; td++) {
                var currentTd = this.tableBody.children[tr].children[td];
                currentTd.classList.remove("selectedDay", "selectableDay");
                // currentTd.removeEventListener("click", calendar.selectDay);
                currentTd.innerHTML = "";
            }
        }

        // Affiche dans chaque cellule le jour
        tabMois.forEach(jour => {
            var numDay = jour.getDay();
            var tr = this.tableBody.children["line" + line];
            var td = tr.children["col" + numDay];

            // Ecrit le jour
            td.innerHTML = day;

            // Stocke dans la cellule le jour;
            td.day = jour;

            // Rajoute la fonction sur pour sélectionner sur la cellule
            td.addEventListener("click", function () { calendar.selectDay(calendar, this) });
            td.classList.add("selectableDay");
            tr.classList.remove("hidden");

            if (day == this.selectedDay.getDate()) {
                td.classList.add("selectedDay");
            }
            // Va à la ligne suivante quand on arrive à dimanche
            if (numDay == 0) {
                line++;
            }

            day++;
        });
    }

    /**
     * https://www.w3resource.com/javascript-exercises/javascript-date-exercise-3.php
     */
    /**
     * Retourne le nombr de jour dans un mois
     * @param {Int} month mois
     * @param {Int} year année
     * @return {Int} Le nombre de jour qu'il y a dans un mois
     */
    getDaysInMonth(month, year) {
        // Here January is 0 based
        // Day 0 is the last day in the previous month
        return new Date(year, month + 1, 0).getDate();
    };

    /**
     * Sélectionne un jour
     * @param {Calendrier} calendar calendrier qui subit une action
     * @param {HTMLElement} sender élément qui appelle la fonction
     * @return {void}
     */
    selectDay(calendar, sender) {
        // Enlève l'autre jour sélectionné
        calendar.restartSelectDay(calendar);
        // Change le jor sélectionné
        calendar.selectedDay = sender.day;
        // Ajoute un classe pour un style différent sur le jour sélectionné
        sender.classList.add("selectedDay");
    }

    /**
     * Enlève tous les select day des éléments
     * @param {Calendrier} calendar calendrier qui subit l'action
     * @return {void}
     */
    restartSelectDay(calendar) {
        for (let tr = 0; tr < calendar.tableBody.childElementCount; tr++) {
            for (let td = 0; td < calendar.tableBody.children[tr].childElementCount; td++) {
                calendar.tableBody.children[tr].children[td].classList.remove("selectedDay");
            }
        }
    }

    /**
     * Retourne en arrière d'un mois à l'appui d'un bouton
     * @param {Calendirer} calendar calendrier qui va être modifier
     * @return {void}
     */
    moveBackMonth(calendar) {
        calendar.changeMonth(calendar, -1);
    }

    /**
     * Avance d'un mois à l'appui d'un bouton
     * @param {Calendirer} calendar calendrier qui va être modifier
     * @return {void}
     */
    moveForwardMonth(calendar) {
        calendar.changeMonth(calendar, +1);
    }

    /**
     * Permet de changer de mois en fonction d'une valeur
     * @param {Calendrier} calendar calendrier sur lequel on fait une action
     * @param {Int} value valeur du changement de mois
     * @return {void}
     */
    changeMonth(calendar, value) {
        calendar.selectedDay.setMonth(calendar.selectedDay.getMonth() + value);
        calendar.afficherMois(calendar.getMois(calendar.selectedDay), calendar);
    }

    /**
     * Recule d'un jour
     * @param {Calendrier} calendar objet calendrier
     * @param {HTMLElement} block élément dans lequel on écrit
     * @return {void}
     */
    moveBackDay(calendar, block) {
        this.changeDay(calendar, block, -1);
    }

    /**
     * Avance d'un jour
     * @param {Calendrier} calendar objet calendrier
     * @param {HTMLElement} block élément dans lequel on écrit
     * @return {void}
     */
    moveForwardDay(calendar, block) {
        this.changeDay(calendar, block, +1);
    }

    /**
     * Change de jour
     * @param {Calendrier} calendar objet calendrier
     * @param {HTMLElement} block élément dans lequel on écrit
     * @param {Int} value valeur qui change de jour
     * @return {void}
     */
    changeDay(calendar, block, value) {
        calendar.selectedDay.setDate(calendar.selectedDay.getDate() + value);
        block.innerHTML = this.writeDay();
    }

    /**
     * Initialise les fonctions des boutons droite et gauche pour changer de mois
     * @param {Calendrier} calendar objet calendrier
     * @param {HTMLElement} leftButton bouton gauche
     * @param {HTMLElement} rightButton bouton droit
     * @return {void}
     */
    initButtonMonth(calendar, leftButton, rightButton) {
        leftButton.addEventListener("click", function () { calendar.moveBackMonth(calendar); });
        rightButton.addEventListener("click", function () { calendar.moveForwardMonth(calendar); });
    }

    /**
     * Initialise les fonctions des boutons droite et gauche pour changer de jour
     * @param {Calendrier} calendar objet calendrier
     * @param {HTMLElement} leftButton bouton gauche
     * @param {HTMLElement} rightButton bouton droit
     * @param {HTMLElement} block élément dans lequel on écrit
     * @return {void}
     */
    initButtonDay(calendar, leftButton, rightButton, block) {
        leftButton.addEventListener("click", function () { calendar.moveBackDay(calendar, block); });
        rightButton.addEventListener("click", function () { calendar.moveForwardDay(calendar, block); });
    }

    /**
     * Crée un cookie ous form JSON
     * @return {string} nom du cookie
     */
    createCookie() {
        return "selectedDay=" + this.selectedDay.toString();
    }

    /**
     * Ecrit le jour
     * @return {string} le jour
     */
    writeDay() {
        return this.tabJours[this.selectedDay.getDay().toString()] + " - " + this.selectedDay.getDate().toString() + " " + this.mois[this.selectedDay.getMonth().toString()] + " " + this.selectedDay.getFullYear().toString();
    }

    /**
     * Change de jour
     * @param {Date} newDate nouvelle date
     * @param {Calendrier} calendar objet calendrier
     * @return {void}
     */
    setDay(newDate, calendar) {
        calendar.selectedDay = new Date(newDate);
    }

    /**
     * Ecrit la date d'une manière formatté
     * @return {string} date formattée
     * @return {void}
     */
    getFormattedDate() {
        return this.selectedDay.getFullYear() + "-" + (this.selectedDay.getMonth() < 10 ? "0" : "") + (this.selectedDay.getMonth() + 1) + "-" + (this.selectedDay.getDate() < 10 ? "0" : "") + this.selectedDay.getDate();
    }
}