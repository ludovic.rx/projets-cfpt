//   Projet    :   Friend Plan
//   Auteur    :   Ludovic Roux et Dan Bonvallat
//   Desc.     :   Fichier js qui s'occupe de faire foncztionner le switch
//   Version   :   1.0, 26.12.2020, LR et DB, version initiale

/**
 * Gère le switch pour le temps de préparation
 * @return {void}
 */
function afficheSwitchTempsPrepa() 
{    
    handleSwitch('swTempPrep', 'tempsPrepa');
}

/**
 * Gère le switch pour le trajet retour
 * @return {void}
 */
function afficheHeureDepartTrajetRertour()
{
    handleSwitch('swTrajetRetour', 'heureTrajetRetour');
}

/**
 * Gère un switch
 * @param {string} swName nom du switch
 * @param {string} divName nom de la div qu'on affiche
 * @return {void}
 */
function handleSwitch(swName, divName) {
    if (document.getElementById(swName).checked) 
    {
        document.getElementById(divName).style.visibility = 'visible';
    }
    else
    {
        document.getElementById(divName).style.visibility = 'hidden';
    }
}