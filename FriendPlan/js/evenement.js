//   Projet    :   Friend Plan
//   Auteur    :   Ludovic Roux et Dan Bonvallat
//   Desc.     :   Class evenement qui permet de gérer un évenement qui se produit dans la journée
//   Version   :   1.0, 07.10.2020, LR et DB, version initiale

/**
 * Classe de base pour un événement
 */
class Evenement {
    /**
     * Crée un événement
     * @param {string} name nom de l'événement
     * @param {string} timeStart heure de commencemeent
     * @param {string} timeEnd heure de fin
     * @param {string} day jour
     */
    constructor(name, timeStart, timeEnd, day) {
        this.name = name;
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
        this.day = day;
    }

    /**
     * Returns the evnts as an array
     * @param {string} jsonList list of events
     * @return {string} list of events as json array
     */
    static getListEvents(jsonList) {
        return JSON.parse(jsonList);
    }

    /**
     * Get the events of this day
     * @param {string} day current day
     * @param {Array} events events 
     * @return {array} result of the events of this day
     */
    static getEventsOfDay(day, events) {
        let result = new Array();
        events.forEach(event => {
            let dayEvent = new Date(event.day);
            if (dayEvent.getFullYear() == day.getFullYear() && dayEvent.getMonth() == day.getMonth() && dayEvent.getDate() == day.getDate()) {
                result.push(event);
            }
        });

        return result;
    }

    /**
     * Trie les événements
     * @param {array} events events
     * @return {array} events qui sont triés
     */
    static sortEvents(events) {
        let unsorted = true;
        while (unsorted) {
            unsorted = false;
            for (let index = 0; index < events.length - 1; index++) {
                if (events[index].timeStart > events[index + 1].timeStart) {
                    let temp = events[index];
                    events[index] = events[index + 1];
                    events[index + 1] = temp;
                    unsorted = true
                }
            }
        }

        return events;
    }

    /**
     * Obtient les latitudes et longitude de l'adresse
     * @param {string} adress adresse recherchée
     * @return {object} objet qui contient latitude et longitude
     */
    static getLatLng(adress) {
        let response;
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.open("GET", 'https://api.opencagedata.com/geocode/v1/json?key=a25ffd3ce762478eb5c2302aeeb80aba&q=' + adress + '&pretty=1&no_annotations=1', false);

        xmlHttp.onload = function () {
            // see full list of possible response codes:
            // https://opencagedata.com/api#codes

            if (xmlHttp.status == 200) {
                // Success!
                var data = JSON.parse(xmlHttp.responseText);
                response = data.results[0].geometry;

            } else if (xmlHttp.status <= 500) {
                // We reached our target server, but it returned an error

                console.log("unable to geocode! Response code: " + xmlHttp.status);
                var data = JSON.parse(xmlHttp.responseText);
                console.log(data.status.message);
            } else {
                console.log("server error");
            }
        };

        xmlHttp.onerror = function () {
            // There was a connection error of some sort
            console.log("unable to connect to server");
        };

        xmlHttp.send();  // make the request

        return response;
    }
}

/**
 * Classe pour un événement sans trajet  
 */
class EvenementWhitoutDrive extends Evenement {
    /**
     * Crée un événement sans trajet
     * @param {string} name nom de l'vénement
     * @param {string} timeStart heure de commencement
     * @param {string} timeEnd heure der gin
     * @param {string} adress adresse
     * @param {string} day jour
     */
    constructor(name, timeStart, timeEnd, adress, day) {
        super(name, timeStart, timeEnd, day);

        this.adress = adress;
        this.latlngs = [Evenement.getLatLng(adress)];
    }
}

/**
 * Evenement avec trajet
 */
class EvenementWithDrive extends Evenement {
    /**
     * Crée un événement avec un trajet
     * @param {string} name nom de l'événement
     * @param {string} timeStart heure de commecemenet
     * @param {string} timeEnd heure de fin
     * @param {string} day jour
     * @param {string} adressStart adrese de départ
     * @param {string} adressEnd adresse de fin
     * @param {string} timePreparation temps de preparation avant le trajet
     * @param {string} timeReturnDrive heure du trajet retour
     */
    constructor(name, timeStart, timeEnd, day, adressStart, adressEnd, timePreparation, timeReturnDrive) {
        super(name, timeStart, timeEnd, day);

        this.adressStart = adressStart;
        this.adressEnd = adressEnd;
        
        this.timePreparation = timePreparation;
        this.timeReturnDrive = timeReturnDrive;

        this.latlngs = [Evenement.getLatLng(adressStart), Evenement.getLatLng(adressEnd)];
    }
}

