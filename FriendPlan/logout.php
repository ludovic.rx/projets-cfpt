<?php
//    Projet    :   Friend Plan
//    Auteur    :   Ludovic Roux et Dan Bonvallat
//    Desc.     :   Déconnecte l'utilisateur
//    Version   :   1.0, 26.12.2020, LR et DB, version initiale

require_once "php/session.inc.php";
require_once "php/functions.inc.php";

$deconect = "";
$stay = "";

validInput($deconect, INPUT_POST, 'deconect', FILTER_SANITIZE_STRING, FILTER_DEFAULT);
validInput($stay, INPUT_POST, 'stay', FILTER_SANITIZE_STRING, FILTER_DEFAULT);

if(isset($deconect))
{
    $_SESSION = null;
    session_destroy();
    header("Location: signin.php");
}
elseif(isset($stay))
{
    header("Location: index.php");
}


?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Logout</title>

    <!-- CSS -->
    <link href="https://getbootstrap.com/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/sign-in/">

    <!-- Bootstrap core CSS -->
    <meta name="theme-color" content="#563d7c">

    <!-- Custom styles for this template -->
    <link href="https://getbootstrap.com/docs/4.5/examples/sign-in/signin.css" rel="stylesheet">
    <link href="style/signin.css" rel="stylesheet">
    <link rel="icon" href="assets/icon_friend_plan.PNG"/>

</head>

<body class="text-center">
    <form class="form-signin" method="POST" , action="#">
        <img class="mb-4 mr-2 ml-2" src="assets/logo_friend_plan_poster.png" alt="logo friend plan" width="200" height="200">
        <h1 class="h3 mb-3 font-weight-normal">Voulez-vous vous déconnectez ?</h1>
        <input class="btn btn-lg btn-light btn-block " type="submit" name="deconect" value="Oui" />
        <input class="btn btn-lg btn-light btn-block " type="submit" name="stay" value="Non" />
    </form>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    
</body>

</html>