<!--
*    Projet    :   Friend Plan
*    Auteur    :   Ludovic Roux et Dan Bonvallat
*    Desc.     :   Page de création de compte
*    Version   :   1.0, 14.10.2020, LR et DB, version initiale
-->
<!DOCTYPE html>
<html lang="fr">
<?php
require_once("php/functions.inc.php");

$email  = "";
$firstName = "";
$name = "";
$classFirstName = "";
$className = "";
$validEmail = true;
$classEmail = "";
$password = "";
$submit = "";

$classPassword = "";
$confirmPassword = "";
$classConfirmPassword = "";

$errorMessage = "";

$email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_EMAIL);
if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $validEmail = false;
}


$password = filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING);
$confirmPassword = filter_input(INPUT_POST, "confirmPassword", FILTER_SANITIZE_STRING);
$firstName = filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING);
$name = filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRING);
$submit = filter_input(INPUT_POST, "submit", FILTER_SANITIZE_STRING);


if ($submit) {
    if ($email && $password && $confirmPassword && $firstName && $name) {
        if ($validEmail && $password == $confirmPassword) {
            // Insérer fonction rajout utilisateur
            if(createUser($email, $password, $name, $firstName)) {
                header("Location: signin.php");
                exit();
            }
        } else {
            if (!$validEmail) {
                $classEmail = "is-invalid";
                $errorMessage .= "L'email n'est pas valide. Veuillez le retaper";
            }

            if ($password !== $confirmPassword) {
                $classPassword = "is-invalid";
                $classConfirmPassword = "is-invalid";
                $errorMessage .= "Les deux mots de passes ne sont pas identiques. ";
            }
        }
    } else {
        if (!$email) {
            $classEmail = "is-invalid";
        }

        if (!$password) {
            $classPassword = "is-invalid";
        }

        if (!$confirmPassword) {
            $classConfirmPassword = "is-invalid";
        }
        if(!$firstName)
        {
            $classFirstName = "is-invalid";
        }
        if(!$name)
        {
            $className = "is-invalid";
        }
    }
}

?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Créer Compte</title>

    <!-- CSS -->
    <link href="https://getbootstrap.com/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/sign-in/">

    <!-- Bootstrap core CSS -->
    <meta name="theme-color" content="#563d7c">

    <!-- Custom styles for this template -->
    <link href="https://getbootstrap.com/docs/4.5/examples/sign-in/signin.css" rel="stylesheet">
    <link href="style/signin.css" rel="stylesheet">
    <link rel="icon" href="assets/icon_friend_plan.PNG"/>
</head>

<body class="text-center">
    <form class="form-signin" method="POST" action="#">
        <img class="mb-4 mr-2 ml-2" src="assets/logo_friend_plan_poster.png" alt="logo friend plan" width="200" height="200">
        <h1 class="h3 mb-3 font-weight-normal">Veuillez entrer vos informations pour la création du compte</h1>
        
        <label for="inputEmail" class="sr-only">Adresse email</label>
        <input type="email" id="inputEmail" class="form-control <?= $classEmail ?>" placeholder="Adresse email" name="email" required autofocus value="<?= $email ?>">


        <label for="inputPassword" class="sr-only">Mot de passe</label>
        <input type="password" id="inputPassword" class="form-control midInput <?= $classPassword ?>" placeholder="Mot de passe" name="password" required>


        <label for="inputPassword" class="sr-only">Mot de passe confirmation</label>
        <input type="password" id="inputPasswordConfirm" class="form-control midInput <?= $classConfirmPassword ?>" placeholder="Confirmer mot de passe" name="confirmPassword" required>

        <label for="inputFirstName" class="sr-only">Prénom</label>
        <input type="text" id="inputFirstName" class="form-control midInput <?= $classFirstName?>" name="firstname" value="<?= $firstName?>" placeholder="Prénom" required>

        <label for="inputName" class="sr-only">Nom</label>
        <input type="text" name="name" id="inputName" class="form-control lastInput <?= $className?>" value="<?= $name?>" placeholder="Nom de famille" required>

        <br><br>
        <div class="invalid-feedback mt-2 mb-2"><?= $errorMessage ?></div>
        <input class="btn btn-lg btn-light btn-block" type="submit" name="submit" value="Créer le compte"></input>
        
    </form>
</body>

</html>