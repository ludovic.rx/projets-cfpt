<?php
//    Projet    :   Friend Plan
//    Auteur    :   Ludovic Roux et Dan Bonvallat
//    Desc.     :   Page de log in
//    Version   :   1.0, 14.10.2020, LR et DB, version initiale
require_once("php/session.inc.php");
require_once("php/functions.inc.php");

$submit = "";
$email = "";
$password = "";
validInput($submit, INPUT_POST, 'submit', FILTER_SANITIZE_STRING, FILTER_DEFAULT);

if ($submit == "Se connecter") {
    if (
        validInput($email, INPUT_POST, 'email', FILTER_SANITIZE_STRING, FILTER_DEFAULT) &&
        validInput($password, INPUT_POST, 'password', FILTER_SANITIZE_STRING, FILTER_DEFAULT)
    ) {
        if (connectUser($email, $password)) {
            $_SESSION["idUser"] = getUserByEmail($email)["idUser"];
            header("Location: index.php");
            exit();
        } else {
            print("Le mot de passe ou l'adresse email est incorect");
        }
    }
}

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>Signin</title>

    <!-- CSS -->
    <link href="https://getbootstrap.com/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/sign-in/">
    <link rel="icon" href="assets/icon_friend_plan.PNG"/>

    <!-- Bootstrap core CSS -->
    <meta name="theme-color" content="#563d7c">

    <!-- Custom styles for this template -->
    <link href="https://getbootstrap.com/docs/4.5/examples/sign-in/signin.css" rel="stylesheet">
    <link href="style/signin.css" rel="stylesheet">

</head>

<body class="text-center">
    <form class="form-signin" method="POST" , action="#">
        <img class="mb-4 mr-2 ml-2" src="assets/logo_friend_plan_poster.png" alt="logo friend plan" width="200" height="200">
        <h1 class="h3 mb-3 font-weight-normal">Veuillez vous connecter</h1>
        <label for="inputEmail" class="sr-only">Adresse email</label>
        <input type="email" id="inputEmail" class="form-control" placeholder="Adresse email" name="email" required autofocus>
        <label for="inputPassword" class="sr-only">Mot de passe</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Mot de passe" name="password" required>
        <div class="mb-2">
            <a href="createAccount.php">Voulez-vous créer un compte ?</a>
        </div>
        <input class="btn btn-lg btn-light btn-block " type="submit" name="submit" value="Se connecter" />
    </form>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script>
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
</body>

</html>