<!--
*    Projet    :   Friend Plan
*    Auteur    :   Ludovic Roux et Dan Bonvallat
*    Desc.     :   Page d'ajout
*    Version   :   1.0, 16.09.2020, LR et DB, version initiale
-->
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="style/main.css">
    <link rel="icon" href="assets/icon_friend_plan.PNG"/>

    <title>Ajout</title>
</head>

<body>
    <div class="divMain">
        <?php include_once("./php/header.inc.php"); ?>
        <main class="mainAjout">
            <div class="date" id="date">
                <h2 id="jour">Mercredi: </h2>
                <input type="date" id="choixDate" name="trip-start" value="2020-09-16" min="2000-01-01" max="2099-12-31">
            </div>
            <button class="btnTrajet" id="btnWithTrajet" onclick="window.location.href='withTrajet.php'">
                Evenement avec trajet
            </button>
            <button class="btnTrajet" id="btnWithoutTrajet" onclick="window.location.href='withoutTrajet.php'">
                Evenement sans trajet
            </button>
        </main>
        <footer>
            <button class="fullSize" onclick="window.location.href='plan.php'" id="btnRetour">Retour</button>
        </footer>
    </div>
</body>
<script src="js/calendrier.js"></script>
<script src="js/cookie.js"></script>
<script>
    document.getElementById("jour").innerHTML = cal.writeDay();
    document.getElementById("choixDate").value = cal.getFormattedDate();

    document.getElementById("btnRetour").addEventListener("click", function() {
        document.cookie = cal.createCookie()
    });

    document.getElementById("choixDate").addEventListener("change", function() {
        cal.setDay(this.value, cal);
        document.getElementById("jour").innerHTML = cal.writeDay();
    });
</script>

</html>