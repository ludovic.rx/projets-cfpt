<!--
*    Projet    :   Friend Plan
*    Auteur    :   Ludovic Roux et Dan Bonvallat
*    Desc.     :   Page d'ajout
*    Version   :   1.0, 16.09.2020, LR et DB, version initiale
-->
<?php

include_once("./php/functions.inc.php");


$submit = filter_input(INPUT_POST, "submit", FILTER_SANITIZE_STRING);
$nom = "";
$adresseDepart = "";
$adresseArrive = "";
$heureDebut = "";
$heureFin = "";
$tempsPrepa = "00:00";
$heureTrajetRetour = "";
$date = "";

$newEvent = array();
$eventsUser = null;

$switchTempsPrepa = filter_input(INPUT_POST, 'switchTempsPrepa', FILTER_SANITIZE_STRING);
$switchTrajetRetour = filter_input(INPUT_POST, 'switchTrajetRetour', FILTER_SANITIZE_STRING);


if (
    validInput($nom, INPUT_POST, 'nomEvenement', FILTER_SANITIZE_STRING, FILTER_DEFAULT) &&
    validInput($adresseDepart, INPUT_POST, 'nomAdresseDepart', FILTER_SANITIZE_STRING, FILTER_DEFAULT) &&
    validInput($adresseArrive, INPUT_POST, 'nomAdresseArrive', FILTER_SANITIZE_STRING, FILTER_DEFAULT) &&
    validInput($heureDebut, INPUT_POST, 'debutHeure', FILTER_SANITIZE_STRING, FILTER_DEFAULT) &&
    validInput($heureFin, INPUT_POST, 'finHeure', FILTER_SANITIZE_STRING, FILTER_DEFAULT) &&
    validInput($tempsPrepa, INPUT_POST, 'tempsPrepa', FILTER_SANITIZE_STRING, FILTER_DEFAULT) &&
    validInput($heureTrajetRetour, INPUT_POST, 'heureTrajetRetour', FILTER_SANITIZE_STRING, FILTER_DEFAULT) &&
    validInput($date, INPUT_POST, "date", FILTER_SANITIZE_STRING, FILTER_DEFAULT)
) {
    $newEvent = array("nom" => $nom, "adresseDepart" => $adresseDepart, "adresseArrive" => $adresseArrive, "heureDebut" => $heureDebut, "heureFin" => $heureFin, "tempsPrepa" => $tempsPrepa, "heureTrajetRetour" => $heureTrajetRetour, "date" => $date);
    $eventsUser = getEvent($_SESSION["idUser"])["events"];
    if ($eventsUser == "") {
        $eventsUser = null;
    }
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <link rel="stylesheet" href="style/main.css">
    <link rel="icon" href="assets/icon_friend_plan.PNG" />

    <title>Avec trajet</title>
</head>

<body>
    <div class="divMain">
        <?php include_once("./php/header.inc.php"); ?>

        <main>
            <form action="#" method="POST" id="form">
                <div class="date" id="date">
                    <h2 id="jour">Mercredi 2 février</h2>
                    <input type="date" id="choixDate" class="start" name="date" value="2020-09-23" min="2000-01-01" max="2099-12-31">
                </div>
                <div class="form-input">
                    <div>
                        <input type="text" name="nomEvenement" placeholder="Nom de l'évenement" value="<?php echo $nom ?>">
                    </div>
                    <div>
                        <label>Adresse départ</label>
                        <input type="text" name="nomAdresseDepart" placeholder="Adresse de départ" value="<?php echo $adresseDepart ?>">
                    </div>
                    <div>
                        <label>Adresse arrivée</label>
                        <input type="text" name="nomAdresseArrive" placeholder="Adresse d'arrivée" value="<?php echo $adresseArrive ?>">
                    </div>
                    <div>
                        <label class="lblHeure">Début :</label>
                        <input type="time" name="debutHeure" min="00:00" max="23:59" value="<?php echo $heureDebut ?>">
                    </div>
                    <div>
                        <label class="lblHeure">Fin :</label>
                        <input type="time" name="finHeure" min="00:00" max="23:59" value="<?php echo $heureFin ?>">
                    </div>
                    <div>
                        <label class="labelSwitch">
                            Temps de préparation
                            <sup><i class="far fa-question-circle" data-toggle="tooltip" data-placement="top" title="Permet d'ajouter un temps de préparation avant le départ du trajet." data-original-title="Permet d'ajouter un temps de préparation avant le départ du trajet."></i></sup>
                            <div class="divSwitch">
                                <label class="switch">
                                    <input type="checkbox" id="swTempPrep" name="switchTempsPrepa" value="active" <?php echo $switchTempsPrepa == 'active' ? "" : "" ?> onclick="afficheSwitchTempsPrepa()">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </label>

                        <input type="time" class="inputSwitch" id="tempsPrepa" name="tempsPrepa" min="00:00" max="12:00" value="<?php echo $tempsPrepa ?>">
                    </div>
                    <div>
                        <label class="labelSwitch">Trajet retour <sup><i class="far fa-question-circle" data-toggle="tooltip" data-placement="top" title="Active le trajet retour." data-original-title="Active le trajet retour.">
                                </i></sup>
                            <div class="divSwitch">
                                <label class="switch">
                                    <input type="checkbox" id="swTrajetRetour" value="active" <?php echo $switchTrajetRetour == 'active' ? "" : "" ?> onclick="afficheHeureDepartTrajetRertour()">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </label>
                        <!-- <label>Heure de départ du trajet retour</label> -->
                        <input type="time" class="inputSwitch" id="heureTrajetRetour" name="heureTrajetRetour" min="00:00" max="23:59" value="<?php echo $heureTrajetRetour ?>">
                    </div>
                </div>
            </form>
        </main>
        <footer>
            <button class="halfSize" onclick="window.location.href='ajout.php'" id="btnRetour">Retour</button>
            <button class="halfSize" id="btnValider" type="submit" name="submit" onclick='document.getElementById("form").submit();'>Valider</button>
        </footer>

    </div>
</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/72d0021155.js" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://getbootstrap.com/docs/4.5/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<script>
    $(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>

<script src="js/switch.js"></script>
<script src="js/calendrier.js"></script>
<script src="js/cookie.js"></script>
<script src="js/evenement.js"></script>
<script>
    document.getElementById("jour").innerHTML = cal.writeDay();
    document.getElementById("choixDate").value = cal.getFormattedDate();

    document.getElementById("btnRetour").addEventListener("click", function() {
        document.cookie = cal.createCookie()
    });


    document.getElementById("choixDate").addEventListener("change", function() {
        cal.setDay(this.value, cal);
        document.getElementById("jour").innerHTML = cal.writeDay();
    });

    function addEvenement() {
        document.getElementById("form").submit();
    }


    if (<?= $newEvent == array() ? "false" : "true" ?>) {
        let values = <?= json_encode($newEvent) ?>;
        let events = JSON.parse(<?= json_encode($eventsUser) ?>);
        let evenement = new EvenementWithDrive(values.nom, values.heureDebut, values.heureFin, new Date(values.date), values.adresseDepart, values.adresseArrive, values.tempsPrepa, values.heureTrajetRetour);
        if (events == null) {
            events = new Array();
        }
        events.push(evenement);
        document.cookie = "events=" + JSON.stringify(events);
        window.location.href = 'addEvent.php?typeEvent=0';
    }
</script>

</html>