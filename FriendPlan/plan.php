<!DOCTYPE html>
<?php
//   Projet    :   Friend Plan
//   Auteur    :   Ludovic Roux et Dan Bonvallat
//   Desc.     :   Page d'exemple
//   Version   :   1.0, 16.09.2020, LR et DB, version initiale

include_once("php/session.inc.php");
include_once("php/functions.inc.php");
include_once("php/permmisions.inc.php");

?>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="style/main.css">
    <link rel="icon" href="assets/icon_friend_plan.PNG"/>

    <title>Plan</title>
</head>

<body>
    <div class="divMain">
        <?php include_once("./php/header.inc.php"); ?>
        <main>
            <div class="mois" id="mois">
                <button type="button" class="changeMonth" id="btnLeft">&#60;</button>
                <h2 id="jour" class="fullDay">5 Février 2020</h2>
                <button type="button" class="changeMonth" id="btnRight">&#62;</button>
            </div>
            <div class="classResolution">
                <div id="mapid" class="map"></div>
                <div class="event">
                    <table class="classTable" id="tableEvents">
                        <thead>
                            <tr>
                                <th>Time</th>
                                <th>Event</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </main>
        <footer>
            <button class="halfSize" onclick='window.location.href="index.php"' id="btnRetour">Retour</button>
            <button class="halfSize" onclick='window.location.href="ajout.php"' id="btnAdd">Ajouter un événement</button>

        </footer>
    </div>
</body>
<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>
<script src="https://cdn.jsdelivr.net/gh/opencagedata/leaflet-opencage-search@1.3.0/dist/js/L.Control.OpenCageSearch.min.js"></script>
<script src="js/carte.js"></script>
<script src="js/evenement.js"></script>
<script src="js/calendrier.js"></script>
<script src="js/cookie.js"></script>
<script src="js/plan.js"></script>
<script>
    let table = document.getElementById("tableEvents");
    let events = <?= json_encode(getEvent($_SESSION["idUser"])["events"]) ?>;

    document.getElementById("mois").children[1].innerHTML = cal.writeDay();
    cal.initButtonDay(cal, document.getElementById("btnLeft"), document.getElementById("btnRight"), document.getElementById("jour"));

    if (events) {
        addEvents(table, events, cal);
        intiButtonDayPlan(document.getElementById("btnLeft"), document.getElementById("btnRight"), table, events, cal);
    } else {
         // table.innerHTML = "";
    }

     // Buttons
    document.getElementById("btnRetour").addEventListener("click", function() {
        document.cookie = cal.createCookie()
    });
    document.getElementById("btnAdd").addEventListener("click", function() {
        document.cookie = cal.createCookie()
    });
</script>

</html>