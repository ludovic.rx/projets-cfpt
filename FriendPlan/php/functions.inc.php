<?php
// Projet    :   FriendPlan
// Auteur    :   Ludovic Roux
// Desc.     :   Fonctions du projet
// Version   :   1.0, 28.10.2020, LR et DB, version initiale
include_once("php/database.inc.php");
include_once("php/session.inc.php");

/**
 * Select a user
 *
 * @param string $email email researched
 * @return false|array array or False if error
 */
function getUserByEmail($email)
{
    static $ps = null;
    $db = connectDB();
    $sql = "SELECT * FROM `users` WHERE `email` LIKE :EMAIL";

    $answer = false;
    try {
        if ($ps == null) {
            // prepare analyse la requête pour savoir s'il peut la résoudre (correction syntaxique, analyse table champs, calule le cout de la requete)
            $ps = $db->prepare($sql);
        }

        $ps->bindParam(':EMAIL', $email, PDO::PARAM_STR);

        if ($ps->execute()) {
            $answer = $ps->fetch(PDO::FETCH_ASSOC);
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
    }

    return $answer;
}

/**
 * Select a user thanks to idUser
 *
 * @param string $iduser id of user researched
 * @return false|array array or False if error
 */
function getUserById($idUser)
{
    static $ps = null;
    $db = connectDB();
    $sql = "SELECT * FROM `users` WHERE `idUser` LIKE :IDUSER";

    $answer = false;
    try {
        if ($ps == null) {
            // prepare analyse la requête pour savoir s'il peut la résoudre (correction syntaxique, analyse table champs, calule le cout de la requete)
            $ps = $db->prepare($sql);
        }

        $ps->bindParam(':IDUSER', $idUser, PDO::PARAM_INT);

        if ($ps->execute()) {
            $answer = $ps->fetch(PDO::FETCH_ASSOC);
        }
    } catch (PDOException $e) {
        echo $e->getMessage();
    }

    return $answer;
}

/**
 * Connect user
 *
 * @param string $email
 * @param string $password
 * @return bool
 */
function connectUser($email, $password)
{
    $user = getUserByEmail($email);
    if (password_verify($password, $user['password'])) {
        return true;
    } else {
        return false;
    }
}


/**
 * Insert a user in the database
 *
 * @param string $email
 * @param string $password
 * @param string $name
 * @param string $firstName
 * @return bool
 */
function createUser($email, $password, $name, $firstName)
{
    static $ps = null;
    $sql = "INSERT INTO users (`nom`, `prenom`, `email`, `password`) ";
    $sql .= "VALUES (:NOM, :PRENOM, :EMAIL, :PASSWORD)";

    $answer = false;
    try {
        if ($ps == null) {
            $ps = connectDB()->prepare($sql);
        }
        $password = password_hash($password, PASSWORD_BCRYPT);

        $ps->bindParam(':EMAIL', $email, PDO::PARAM_STR);
        $ps->bindParam(':PRENOM', $firstName, PDO::PARAM_STR);
        $ps->bindParam(':NOM', $name, PDO::PARAM_STR);
        $ps->bindParam(':PASSWORD', $password, PDO::PARAM_STR);

        $answer = $ps->execute();
    } catch (Exception $e) {
        echo $e->getMessage();
    }
    return $answer;
}

/**
 * get the values ​​of the event by the usercup   
 * 
 * @param int idUser id of the user
 * @return string result with HTML 
 */
function getEvent($idUser)
{
    static $ps = null;
    $sql = 'SELECT events FROM users';
    $sql .= ' WHERE idUser = :IDUSER';

    if ($ps == null) {
        $ps = connectDB()->prepare($sql);
    }
    $answer = false;
    try {
        $ps->bindParam(':IDUSER', $idUser, PDO::PARAM_INT);

        if ($ps->execute())
            $answer = $ps->fetch(PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        echo $e->getMessage();
    }

    return $answer;
}


/**
 * Update event
 *
 * @param int $idUsers
 * @param string $events
 * @return bool
 */
function updateEvent($idUsers, $events)
{
    static $ps = null;

    $sql = "UPDATE `users` SET ";
    $sql .= "`events` = :EVENTS ";
    $sql .= "WHERE `idUser` = :IDUSER";
    if ($ps == null) {
        $ps = connectDB()->prepare($sql);
    }
    $answer = false;
    try {
        $ps->bindParam(':EVENTS', $events, PDO::PARAM_STR);
        $ps->bindParam(':IDUSER', $idUsers, PDO::PARAM_INT);
        $ps->execute();
        $answer = ($ps->rowCount() > 0);
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
    return $answer;
}


/**
 * Validate an input
 * 
 * @param string $input input to validate
 * @param string $typeInput type of input ex: INPUT_POST
 * @param string $nameInput name of input
 * @param string $filterSanitize FILTER_SANITIZE
 * @param string $filterValidate FILTER_VALIDATE
 * @return bool true if input is valid, otherwise, false
 */
function validInput(&$input, $typeInput, $nameInput, $filterSanitize, $filterValidate)
{
    $input = filter_input($typeInput, $nameInput, $filterSanitize);
    $answer = false;
    if (filter_var($input, $filterValidate)) {
        $answer = true;
    }
    return $answer;
}

/**
 * Verify if the user is connected and display in terms of HTML
 *
 * @return string result with HTML
 */
function verifiyConnection()
{
    $answer = "";
    $idUser = $_SESSION["idUser"];
    if (empty($idUser)) {
        $answer = '<a class="signin" href="signin.php"><h2>Sign In <img src="assets/log-in.svg" alt="" width="16" height="16"></h2></a>';
    } else {
        $user = getUserById($idUser);
        $answer = '<a class="signin" href="logout.php"><h2>Bienvenue ' . $user["prenom"] . '</h2></a>';
    }
    return $answer;
}