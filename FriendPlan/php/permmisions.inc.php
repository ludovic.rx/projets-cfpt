<?php
//   Projet    :   Friend Plan
//   Auteur    :   Ludovic Roux et Dan Bonvallat
//   Desc.     :   Page d'exemple
//   Version   :   1.0, 02.12, LR et DB, version initiale

include_once("session.inc.php");

if(empty($_SESSION["idUser"]) && basename($_SERVER["SCRIPT_FILENAME"])) {
    header("Location: index.php?errorConnection=1");
    exit();
}