<?php
// Projet    :   FriendPlan
// Auteur    :   Ludovic Roux
// Desc.     :   Gère la session
// Version   :   1.0, 11.11.2020, LR et DB, version initiale
if(empty(session_id())) {
    session_start();
}

if (!isset($_SESSION["idUser"])){
    $_SESSION["idUser"] = "";
}