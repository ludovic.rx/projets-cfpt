 <?php
    // Projet    :   url
    // Auteur    :   Ludovic Roux
    // Desc.     :   Constantes du projet notes
    // Version   :   1.0, 3.11.2020, LR, version initiale

    include_once("res/php/database.inc.php");

    /**
     * Insert a site
     *
     * @param string $url url of the site
     * @param string $content content of the site
     * @param Date $date date of the add
     * @return bool true if succeed else false
     */
    function insertSite($url, $content, $date, $title, &$codeError)
    {
        static $ps = null;
        $sql = "INSERT INTO `sites` (`url`, `contenu`, `date`, `title`) VALUES(:URL, :CONTENU, :DATE ,:TITLE)";

        $answer = false;
        $codeError = -1;

        try {
            if ($ps == null) {
                $ps = connectDBUrl()->prepare($sql);
            }
            $content = utf8_encode($content);

            $ps->bindParam(':URL', $url, PDO::PARAM_STR);
            $ps->bindParam(':CONTENU', $content, PDO::PARAM_STR);
            $ps->bindParam(':DATE', $date);
            $ps->bindParam(':TITLE', $title, PDO::PARAM_STR);

            $answer = $ps->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
            $codeError = $e->getCode();
        }

        return $answer;
    }

    /**
     * Get the contents of an url
     *
     * @param string $url url of the site
     * @return string|false content of the site, that is cleared
     */
    function getContent($url, &$title)
    {
        // Ouvre le fichier via l'url
        $handle = fopen($url, "r");
        // Obtient le contenu du site
        $result = stream_get_contents($handle);
        preg_match("/<title.*>.*<\/title>/", $result, $title);
        $title[0] = strip_tags($title[0]);

        // Enlève tout le script
        $result = preg_replace("/<script ?.*>.*<\/script>/sU", "", $result);
        // Enlève les balises de style
        $result = preg_replace("/<style ?.*>.*<\/style>/sU", "", $result);
        // Enlève toutes les nav
        $result = preg_replace("/<nav ?.*>.*<\/nav>/sU", "", $result);


        // Prends le contenu du body
        preg_match("/<body ?.*>.*<\/body>/is", $result, $result);
        // Enlève les balises HTML
        $result[0] = strip_tags($result[0]);

        // Enlèves les espaces inutiles à l'intérieur de la chaîne
        $result[0] = preg_replace("/\s\s+/", " ", $result[0]);

        if (count($result) == 0) {
            return false;
        }
        return $result[0];
    }

    /**
     * Search in the sites that are in the database with the natural language mode
     *
     * @param string $search search 
     * @return array|false 
     */
    function searchInSitesNaturalLanguage($search)
    {
        static $ps = null;
        $sql = "SELECT url, contenu, title, ROUND(MATCH (url,contenu, title) AGAINST (:SEARCH IN NATURAL LANGUAGE MODE), 2) AS score 
        FROM sites 
        WHERE MATCH (url,contenu, title) AGAINST (:SEARCH2 IN NATURAL LANGUAGE MODE) > 0
        ORDER BY score DESC";

        $answer = false;
        try {
            if ($ps == null) {
                $ps = connectDBUrl()->prepare($sql);
            }
            $ps->bindParam(':SEARCH', $search, PDO::PARAM_STR);
            $ps->bindParam(':SEARCH2', $search, PDO::PARAM_STR);
            $ps->execute();

            $answer = $ps->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            $answer = array();
            echo $e->getMessage();
        }

        return $answer;
    }

    /**
     * Search in the sites that are in the database with the natural language mode with query expansion
     *
     * @param string $search search 
     * @return array|false 
     */
    function searchInSitesNaturalLanguageQueryExpansion($search)
    {
        static $ps = null;
        $sql = "SELECT url, contenu, title, ROUND(MATCH (url,contenu, title) AGAINST (:SEARCH IN NATURAL LANGUAGE MODE WITH QUERY EXPANSION), 2) AS score
        FROM sites
        WHERE MATCH (url,contenu, title) AGAINST (:SEARCH2 IN NATURAL LANGUAGE MODE WITH QUERY EXPANSION) > 0
        ORDER BY score DESC";

        $answer = false;
        try {
            if ($ps == null) {
                $ps = connectDBUrl()->prepare($sql);
            }
            $ps->bindParam(':SEARCH', $search, PDO::PARAM_STR);
            $ps->bindParam(':SEARCH2', $search, PDO::PARAM_STR);
            $ps->execute();

            $answer = $ps->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            $answer = array();
            echo $e->getMessage();
        }

        return $answer;
    }

    /**
     * Search in the sites that are in the database
     *
     * @param string $search search 
     * @return array|false 
     */
    function searchInSitesQueryExpansion($search)
    {
        static $ps = null;
        $sql = "SELECT url, contenu, title, ROUND(MATCH (url, contenu, title) AGAINST (:SEARCH WITH QUERY EXPANSION), 2) AS score
        FROM sites
        WHERE MATCH (url,contenu, title) AGAINST (:SEARCH2 WITH QUERY EXPANSION) > 0
        ORDER BY score DESC";

        $answer = false;
        try {
            if ($ps == null) {
                $ps = connectDBUrl()->prepare($sql);
            }
            $ps->bindParam(':SEARCH', $search, PDO::PARAM_STR);
            $ps->bindParam(':SEARCH2', $search, PDO::PARAM_STR);
            $ps->execute();

            $answer = $ps->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            $answer = array();
            echo $e->getMessage();
        }

        return $answer;
    }

    /**
     * Ecrit le résultat de la recherche
     *
     * @param array $result result of the research
     * @return string table in html or a error message
     */
    function showResults($result)
    {
        $answer = "";
        if (count($result) == 0) {
            $answer = "Aucun résultat n'a été trouvé.";
        } else {
            $answer = '<table class="table table-hover">';
            $answer .= "<thead>";
            $answer .= "<tr>";
            $answer .= "<th>Titre</th>";
            $answer .= "<th>Site</th>";
            $answer .= "<th>Score</th>";
            // $answer .= "<th>Score</th>";
            $answer .= "</tr>";
            $answer .= "</thead>";
            $answer .= "<tbody>";
            foreach ($result as $site) {
                $answer .= "<tr>";
                $answer .= '<td>' . $site["title"] . '</td>';
                $answer .= '<td><a href="' . $site["url"] . '">' . $site["url"]  . '</a></td>';
                $answer .= "<td>" .  $site["score"] . "</td>";
                $answer .= "</tr>";
            }
            $answer .= "</tbody>";
            $answer .= "</table>";
        }

        return $answer;
    }

    /**
     * Write a response to the action
     *
     * @param int $codeError code of error
     * @return string ressponse
     */
    function writeResponse($codeError)
    {
        $result = "";
        switch ($codeError) {
            case -1:
                $result = "<div class='valid-feedback d-block text-center w-50 m-auto'>L'insertion a fonctionnée.</div>";
                break;
            case 23000:
                $result = "<div class='invalid-feedback d-block text-center w-50 m-auto'>Le site existe déjà dans la base.</div>";
                break;
        }
        return $result;
    }

    /**
     * Writes the select
     *
     * @return string result
     */
    function writeSelect($selected)
    {
        $valuesSelect = array("Natural Language Mode", "Natural Language With Query Expansion", "With Query Expansion");
        $anwser = '<select class="form-control text-success border-success my-2" name="typeReasearch" aria-labelledby="dropdownMenuLink">';
        foreach ($valuesSelect as $key => $value) {
            $anwser .= '<option onclick="repaceText(this)" value="' . $key . '" ' . ($selected == $key ? "selected" : "") . '>' . $value . '</option>';
        }
        $anwser .= "</select>";
        return $anwser;
    }
