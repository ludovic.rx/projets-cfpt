<?php
// Projet    :   url
// Auteur    :   Ludovic Roux
// Desc.     :   Navigation
// Version   :   1.0, 24.11.2020, LR, version initiale
?>
<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="index.php">URL</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse flex-row-reverse" id="navbarNav">
            <form class="form-inline my-2 my-lg-0 container-fluid justify-content-end" method="POST" action="search.php">
                <input class="form-control mr-sm-2 col-sm-1 col-md-5" type="search" name="search" placeholder="Rechercher dans un site" value="<?= $search ?>" aria-label="Rechercher dans un site">
                <?= writeSelect($typeReasearch) ?>
                <input class="btn btn-outline-success my-2 my-sm-0 mx-1" type="submit" name="btnSubmit" value="Rechercher" />
            </form>
        </div>

    </nav>
</header>
<script>
    function repaceText(sender) {
        sender.parentElement.parentElement.children[0].innerText = sender.innerText
    }
</script>