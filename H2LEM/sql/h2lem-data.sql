-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 11, 2020 at 03:03 PM
-- Server version: 10.3.23-MariaDB-0+deb10u1
-- PHP Version: 7.3.19-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `h2lem`
--
CREATE DATABASE IF NOT EXISTS `h2lem` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `h2lem`;

--
-- Truncate table before insert `coordonnees`
--

TRUNCATE TABLE `coordonnees`;
--
-- Dumping data for table `coordonnees`
--

INSERT INTO `coordonnees` (`id`, `lati`, `longi`, `time`, `idVehicule`) VALUES
(1, 46.1957, 6.11061, '2020-11-11 12:10:00', 1),
(2, 46.1955, 6.11059, '2020-11-11 12:15:00', 1),
(3, 46.1949, 6.10765, '2020-11-11 12:20:00', 1),
(4, 46.194, 6.1082, '2020-11-11 12:25:00', 1),
(5, 46.1939, 6.10817, '2020-11-11 12:30:00', 1),
(6, 46.1938, 6.10831, '2020-11-11 12:35:00', 1),
(7, 46.1941, 6.10904, '2020-11-11 12:40:00', 1),
(8, 46.1948, 6.11379, '2020-11-11 12:45:00', 1),
(9, 46.1954, 6.11626, '2020-11-11 12:50:00', 1),
(10, 46.1965, 6.11698, '2020-11-11 12:55:00', 1),
(11, 46.1963, 6.12066, '2020-11-11 13:00:00', 1),
(12, 46.1941, 6.12454, '2020-11-11 13:05:00', 1),
(13, 46.199, 6.12823, '2020-11-11 13:10:00', 1),
(14, 46.2012, 6.1302, '2020-11-11 13:15:00', 1),
(29, 46.1916, 6.11181, '2020-11-11 12:10:00', 7),
(30, 46.1955, 6.11059, '2020-11-11 12:15:00', 7),
(31, 46.1949, 6.10765, '2020-11-11 12:20:00', 7),
(32, 46.194, 6.1082, '2020-11-11 12:25:00', 7),
(33, 46.1939, 6.10817, '2020-11-11 12:30:00', 7),
(34, 46.1938, 6.10831, '2020-11-11 12:35:00', 7),
(35, 46.1941, 6.10904, '2020-11-11 12:40:00', 7),
(36, 46.1948, 6.11379, '2020-11-11 12:45:00', 7),
(37, 46.1954, 6.11626, '2020-11-11 12:50:00', 7),
(38, 46.1965, 6.11698, '2020-11-11 12:55:00', 7),
(39, 46.1963, 6.12066, '2020-11-11 13:00:00', 7),
(40, 46.1941, 6.12454, '2020-11-11 13:05:00', 7),
(41, 46.199, 6.12823, '2020-11-11 13:10:00', 7),
(42, 46.198, 6.13711, '2020-11-11 13:15:00', 7),
(43, 46.1976, 6.10789, '2020-11-11 12:10:00', 8),
(44, 46.1955, 6.11059, '2020-11-11 12:15:00', 8),
(45, 46.1949, 6.10765, '2020-11-11 12:20:00', 8),
(46, 46.194, 6.1082, '2020-11-11 12:25:00', 8),
(47, 46.1939, 6.10817, '2020-11-11 12:30:00', 8),
(48, 46.1938, 6.10831, '2020-11-11 12:35:00', 8),
(49, 46.1941, 6.10904, '2020-11-11 12:40:00', 8),
(50, 46.1948, 6.11379, '2020-11-11 12:45:00', 8),
(51, 46.1954, 6.11626, '2020-11-11 12:50:00', 8),
(52, 46.1965, 6.11698, '2020-11-11 12:55:00', 8),
(53, 46.1963, 6.12066, '2020-11-11 13:00:00', 8),
(54, 46.1941, 6.12454, '2020-11-11 13:05:00', 8),
(55, 46.199, 6.12823, '2020-11-11 13:10:00', 8),
(56, 46.2012, 6.12503, '2020-11-11 13:15:00', 8),
(57, 46.1976, 6.10789, '2020-11-11 12:10:00', 9),
(58, 46.1955, 6.11059, '2020-11-11 12:15:00', 9),
(59, 46.1949, 6.10765, '2020-11-11 12:20:00', 9),
(60, 46.194, 6.1082, '2020-11-11 12:25:00', 9),
(61, 46.1939, 6.10817, '2020-11-11 12:30:00', 9),
(62, 46.1938, 6.10831, '2020-11-11 12:35:00', 9),
(63, 46.1925, 6.11684, '2020-11-11 12:40:00', 9),
(64, 46.1948, 6.11379, '2020-11-11 12:45:00', 9),
(65, 46.1954, 6.11626, '2020-11-11 12:50:00', 9),
(66, 46.1965, 6.11698, '2020-11-11 12:55:00', 9),
(67, 46.1963, 6.12066, '2020-11-11 13:00:00', 9),
(68, 46.1941, 6.12454, '2020-11-11 13:05:00', 9),
(69, 46.199, 6.12823, '2020-11-11 13:10:00', 9),
(70, 46.1991, 6.12352, '2020-11-11 13:15:00', 9),
(71, 46.1948, 6.11597, '2020-11-11 12:10:00', 10),
(72, 46.1955, 6.11059, '2020-11-11 12:15:00', 10),
(73, 46.1949, 6.10765, '2020-11-11 12:20:00', 10),
(74, 46.194, 6.1082, '2020-11-11 12:25:00', 10),
(75, 46.1939, 6.10817, '2020-11-11 12:30:00', 10),
(76, 46.1938, 6.10831, '2020-11-11 12:35:00', 10),
(77, 46.1925, 6.11684, '2020-11-11 12:40:00', 10),
(78, 46.1948, 6.11379, '2020-11-11 12:45:00', 10),
(79, 46.1954, 6.11626, '2020-11-11 12:50:00', 10),
(80, 46.1965, 6.11698, '2020-11-11 12:55:00', 10),
(81, 46.1963, 6.12066, '2020-11-11 13:00:00', 10),
(82, 46.1941, 6.12454, '2020-11-11 13:05:00', 10),
(83, 46.199, 6.12823, '2020-11-11 13:10:00', 10),
(84, 46.1957, 6.12243, '2020-11-11 13:15:00', 10);

--
-- Truncate table before insert `utilisateurs`
--

TRUNCATE TABLE `utilisateurs`;
--
-- Dumping data for table `utilisateurs`
--

INSERT INTO `utilisateurs` (`idUtilisateur`, `email`, `password`) VALUES
(52, 'bjr@bjr.com', '$2y$10$fCaQ7...720u90SVpEA13erNDs5qIE8nmc1mKVbHzGL4z4gPWwvF6'),
(55, 'dan.bnvll@eduge.ch', '$2y$10$gsgZ7TeCJhgxph.txv6WcuN0VE85jckCqoyUl18Dx5ONA9N/vWP1i'),
(56, 'erik.rpn@eduge.ch', '$2y$10$jXy3GqqKjeG119og78xaT.6fbkq.ttgTY4mCjLY0xldcL2tsgo.u2'),
(57, 'juliano.szlz@eduge.ch', '$2y$10$We4.IftVbXfRK2ZANqHPO.UCQfEF7DSCGHqwSZ.aYacNQVnTSKRC.'),
(58, 'salut@salut.ch', '$2y$10$DMXi6GSTZc3OIdYlkQxqzObcWTafG1oe2qFdgXjFiB38Jf.R9qdhW'),
(59, 'dan@gmail.com', '$2y$10$D6BmvHAsLRcOE41CaoA/UuXMWin./zH6PCkhbAO3BsTGavmk8ceUy'),
(60, 'lroux@live.fr', '$2y$10$qsRzIenUIsEEn9zxeYcIReqXcU6ndVVx7fLBncPP6j3HC6kKRoKym'),
(61, 'lroux@live.frs', '$2y$10$XeVwivAoSFCYwN25Z5dyVeoAkmJ8mkA8ADfzom87PPOLusDLu6DzW');

--
-- Truncate table before insert `vehicules`
--

TRUNCATE TABLE `vehicules`;
--
-- Dumping data for table `vehicules`
--

INSERT INTO `vehicules` (`id`, `nom`, `color`) VALUES
(1, 'Bob', '#3232a8'),
(7, 'Velo 1', '#32a852'),
(8, 'Velo 2', '#7d179c'),
(9, 'Velo 3', '#b51d5f'),
(10, 'Velo 4', '#b01717');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
