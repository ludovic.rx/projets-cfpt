<!-- 
    Projet    :   h2lem4.cfpt.info
    Auteur    :   Ludovic Roux ET Juliano Souza Luz
    Desc.     :   Gère des vélos , retransmet la position lors d'un déplacement. Le trajet est ensuite dessiné sur une carte 
-->
<!DOCTYPE html>
<?php
session_start();
require_once("res/php/functions.php");
require_once("res/php/map.php");
require_once("res/php/constantes.php");


$fullArray = []; // Valeurs des coordonnées des trajets des vélos
$connected = isset($_SESSION["utilisateur"]) && $_SESSION["utilisateur"] != "";

// Filtrage des valeurs recues
$bikeSelected = filter_input(INPUT_GET, "id", FILTER_SANITIZE_STRING); // vélo sélectionné
$date = filter_input(INPUT_GET, "date", FILTER_SANITIZE_STRING); // date sélectionnée
$all = false; // Défini si on choisit d'afficher tous les vélos

// Met la date de base au démarrage
if (!$date) {
    $date = date("Y-m-d");
}
if($connected) {
    if ($bikeSelected != null && $date != null) {
        // Sélectionne tous les vélos par défaut
        if ($bikeSelected == "all") {
            $all = true;
        } else {
            $fullArray = getAdresses($bikeSelected, $date);
        }
    } else {
        $all = true;
    }
}


if ($all) {
    foreach (getVehicles() as $velo) {
        array_push($fullArray, getAdresses($velo["id"], $date));
    }
}

// Récuperation de l'utilisateur connecté. Si aucune connexion, laisse un message pour se connecter
if ($connected) {
    $userTxt = before('@', $_SESSION["utilisateur"]);
    $welcome["Message"] = "Bonjour $userTxt";
    $welcome["Lien"] = 'href="signOut.php"';
} else {
    $welcome["Message"] = '<span data-feather="users"></span> Sign in';
    $welcome["Lien"] = 'href="signIn.php"';
}

if (isset($_POST["welcome"])) {
    $_SESSION["utilisateur"] = "";
}
?>

<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Ludovic Roux, Juliano Souza Luz">
    <meta name="generator" content="Jekyll v4.1.1">
    <meta name="theme-color" content="#563d7c">
    <meta name="msapplication-config" content="https://getbootstrap.com/docs/4.5/assets/img/favicons/browserconfig.xml">
    <title>BikeTracker</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/dashboard/">

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="https://getbootstrap.com/docs/4.5/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="mask-icon" href="https://getbootstrap.com/docs/4.5/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
    
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin="" />
    
<!-- style pour le modal -->
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round" rel="stylesheet">
    <link rel="stylesheet" href="res/css/modal.css">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <!-- Custom styles for this template -->
    <link href="res/css/dashboard.css" rel="stylesheet">
    <link rel="icon" type="image/png" href="res/img/BikeTracker.png" />
</head>

<body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-md-3 col-lg-2 mr-0 px-3" href="#">Bike Tracker</a>
        <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <ul class="navbar-nav px-3">
            <li class="nav-item text-nowrap">
                <a class="nav-link" name="welcome" <?= $welcome["Lien"]; ?>><?= $welcome["Message"]; ?></a>
            </li>
        </ul>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
                <div class="sidebar-sticky pt-3">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <span class="nav-link">
                                <img src="../res/img/speedometer.svg" alt="speedometer" height="24" width="24" class="feather" />
                                <span id="vitesse">Vitesse</span>
                            </span>
                        </li>
                        <li class="nav-item">
                            <span class="nav-link">
                                <img src="../res/img/distance.svg" alt="distance" height="24" width="24" class="feather" />
                                <span id="distance">Distance</span>
                            </span>
                        </li>
                    </ul>

                    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                        <span>Current Bike</span>
                        <a class="d-flex align-items-center text-muted" href="#" aria-label="Add a new report">
                            <span data-feather="plus-circle"></span>
                        </a>
                    </h6>
                    <ul class="nav flex-column mb-2 list-group">
                        <?php
                        $vehicles = getVehicles();
                        $vehicule = $vehicles->fetch(PDO::FETCH_ASSOC);
                        do { ?>
                            <li class="nav-item">
                                <a class="nav-link list-group-item <?= $bikeSelected == $vehicule["id"] ? "active" : "" ?>" onclick='pressLink(this)' href="?id=<?= $vehicule["id"] ?>">
                                    <img src="../res/img/velo.svg" alt="vélo" height="24" width="24" class="feather" />
                                    <span style="color:<?= $vehicule["color"] ?>"><?= $vehicule["nom"] ?></span>
                                </a>
                            </li>
                        <?php
                            $vehicule = $vehicles->fetch(PDO::FETCH_ASSOC);
                        } while ($vehicule != null);
                        ?>
                        <li class="nav-item">
                            <a class="nav-link list-group-item <?= $all ? "active" : "" ?>" onclick='pressLink(this)' href=" ?id=all">
                                <img src="../res/img/velo.svg" alt="vélo" height="24" width="24" class="feather" />
                                <span style="color:black">Tout les vélos</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">Map</h1>
                    <div class="d-flex overflow-auto">
                        <div class="btn-toolbar mb-2 mr-2 mb-md-0">
                            <form class="" method="POST" action="index.php?id=<?= $bikeSelected ?>&&date=<?= getToday() ?>" id="formSearchToday">
                                <input type="submit" class="btn btn-sm btn-outline-secondary" value="Aujourd'hui">
                            </form>
                        </div>
                        <div class="btn-toolbar mb-2 mr-2 mb-md-0">
                            <form class="" method="POST" action="index.php?id=<?= $bikeSelected ?>&&date=<?= getThisWeek() ?>" id="formSearchThisWeek">
                                <input type="submit" class="btn btn-sm btn-outline-secondary" value="Cette semaine">
                            </form>
                        </div>
                        <div class="btn-toolbar mb-2 mr-2 mb-md-0">
                            <form class="" method="POST" action="index.php?id=<?= $bikeSelected ?>&&date=<?= getThisMonth() ?>" id="formSearchThisMonth">
                                <input type="submit" class="btn btn-sm btn-outline-secondary" value="Ce mois">
                            </form>
                        </div>
                        <div class="btn-toolbar mb-2 mr-2 mb-md-0">
                            <form class="" method="POST" action="index.php?id=<?= $bikeSelected ?>&&date=<?= getThisYear() ?>" id="formSearchThisYear">
                                <input type="submit" class="btn btn-sm btn-outline-secondary" value="Cette annéee">
                            </form>
                        </div>
                        <div class="btn-toolbar mb-2 mb-md-0">
                            <input id="date" type="date" class="btn btn-sm btn-outline-secondary dropdown-toggle btn-group mr-2" onchange="changeDate(this, <?= $bikeSelected ?>)" value="<?= $date ?>">
                            <form class="" method="POST" action="index.php?id=<?= $bikeSelected ?>&&date=<?= $date ?>" id="formSearch">
                                <input type="submit" class="btn btn-sm btn-outline-secondary" value="Chercher">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="position-relative">
                    <div class="w-100 chartjs-render-monitor" id="mapid" width="1217" height="800" style="display: block; width: 1217px; height: 800px;"></div>
                    <?php

                    if (!$connected) {
                        include("modal.html");
                    ?>
                        <div class="GreyScreen"></div>
                    <?php } ?>
                </div>
            </main>
        </div>
    </div>
    <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
    <script>
        let varJson = null;
        let all =  <?= $all == true ? "true" : "false" ?>;
        if (<?= $connected ? "true" : "false" ?>) {
            
            varJson = <?php echo json_encode($fullArray) ?>;
        }
    </script>
    <script src="res/js/map.js"></script>
    <script src="res/js/index.js"></script>
</body>

</html>