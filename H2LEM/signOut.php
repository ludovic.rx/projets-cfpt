<!-- 
    Projet    :   h2lem4.cfpt.info
    Auteur    :   Ludovic Roux ET Juliano Souza Luz
    Desc.     :   Permet de se déconnecter 
-->
<!DOCTYPE html>
<?php
session_start();
include_once("res/php/functions.php");

if (isset($_POST["annuler"])) {
  header('Location: index.php');
  exit();
}
if (isset($_POST["deconnecter"])) {
  $_SESSION["utilisateur"] = "";
  header('Location: index.php');
  exit();
}

?>

<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>SignOut</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <link rel="icon" type="image/png" href="res/img/BikeTracker.png" />
  <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/sign-in/">


  <style>
    .bd-placeholder-img {
      font-size: 1.125rem;
      text-anchor: middle;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }

    @media (min-width: 768px) {
      .bd-placeholder-img-lg {
        font-size: 3.5rem;
      }
    }
  </style>
  <!-- Custom styles for this template -->
  <link href="res/css/signIn.css" rel="stylesheet">
</head>

<body class="text-center">
  <form class="form-signin" action="#" method="post">
    <img class="mb-4" src="res/img/BikeTracker.png" alt="" width="72" height="72">
    <h1 class="h3 mb-3 font-weight-normal">You will be disconnected</h1>
    <button class="btn btn-lg btn-danger btn-block" type="submit" name="deconnecter">Sign Out</button>
    <button class="btn btn-lg btn-secondary btn-block" type="submit" name="annuler">Cancel</button>
    <p class="mt-5 mb-3 text-muted">&copy; RX | JSL 2020</p>
  </form>
</body>


</html>