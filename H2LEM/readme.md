# H2LEM

## Introduction
H2LEM est un site web qui répertorie une flotte de vélos.

Ces derniers envoient leurs coordonnées GPS et on peut afficher leur tracé, leur vitesse et la distance parcourue.

## Auteurs
* @ludovic.rx
* @juliano.szlz

## License
GPL v.3

## Installation
L'environnement utilisé est décrit plus tard.

En ce qui concerne la base de donnée, **2 fichiers sql** sont fournis :

* Le fichier ***h2lem-str.sql*** contient la structure de la base de donnée et crée l'utilisateur pour la base de donnée
* Le fichier ***h2lem-data.sql*** contient les données de test 

## Environnement 
* PHP : Version 7.3.19-1~deb10u1
* Serveur Web : NGINX 1.14.2
* Serveur de base de donnée : MariaDB

## Fonctionnalités
* L'utilisateur peut créer un compte et s'y connecter, il ne peut pas utiliser le site lorsqu'il n'a pas de compte
* L'utilisateur peut choisir un vélo
  * La distance parcourue est affichée
  * La vitesse est affichée
  * Il peut choisir l'interval de temps qu'il affiche
  * Le parcours est affiché
* L'utilisateur peut choisir d'afficher tous les vélos en même temps
  * Leurs parcours sont affichés avec des couleurs différentes
  * On peut choisir l'interval de temps