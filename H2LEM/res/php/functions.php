<?php
// Projet    :   h2lem4.cfpt.info
// Auteur    :   Ludovic Roux,Souza Luz Juliano
// Desc.     :   fonctions du projet h2lem4.cfpt.info
// Version   :   1.0, 09.09.2020, LR et JSL, version initiale

use PhpMyAdmin\Console;

include_once("constantes.php");

/**
 * Permet de se conneceter à la base de donnée
 */
function connectDB()
{
    static $myDb = null;

    if ($myDb === null) {
        $myDb = new PDO(
            "mysql:host=" . DB_IP . ";dbname=" . DB_NAME . ";charset=utf8",
            DB_USER,
            DB_PASS,
            [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_EMULATE_PREPARES => false
            ]
        );
    }
    return $myDb;
}

/**
 * Ajouter Coordonnées
 *
 * @param float $lat position du vélo, latitude
 * @param float $long position du élo, longitude
 * @param DateTime $time moment auquel la position est envoyées 
 * @param int $idVehicule id du véhicule donné
 * @return bool true si la query a réussi, false si elle n'a pas réussi
 */
function ajouterCoordonnes($lat, $long, $time, $idVehicule)
{
    try {
        $db = connectDB();
        $sql = "INSERT INTO coordonnees (`lati`, `longi`, `time`, `idVehicule`) VALUES (:lat, :long, :dateTime, :idVehicule)";
        $query = $db->prepare($sql);
        $query->execute([":lat" => $lat, ":long" => $long, ":dateTime" => $time, "idVehicule" => $idVehicule]);

        return true;
    } catch (Exception $e) {
        echo $e;
        return false;
    }
}

/**
 * Récupère tous les points d'un véhicule 
 *
 * @param int $idVehicule Id du véhicule recherché 
 * @param string $date date recherchée
 * @return mixed retourne un array si la requête a réussi, return false si la requête n'a pas réussi 
 */
function getAdresses($idVehicule, $date)
{
    try {
        $db = connectDB();
        $sql = 'SELECT `coordonnees`.`id`, `lati`, `longi`, `time`, `idVehicule`, `color` FROM coordonnees
         JOIN vehicules ON vehicules.id = coordonnees.idVehicule 
         WHERE idVehicule LIKE :idVehicule 
         AND `time` >= :time
         ORDER BY time';
        $query = $db->prepare($sql);
        $query->execute(["idVehicule" => $idVehicule, "time" => $date]);

        return $query->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        return false;
    }
}
/**
 * Récupère et filtre l'input email
 *
 * @param [string] $email
 * @return void
 */
function filtre_email(&$email)
{
    $emailClean = filter_input(INPUT_POST, "email", FILTER_SANITIZE_STRING, FILTER_SANITIZE_EMAIL);
    if ($emailClean) {
        $email = $emailClean;
    } else {
        $email = null;
    }
}
/**
 * Récupère et filtre l'input password
 *
 * @param [string] $password
 * @return void
 */
function filtre_password(&$password)
{
    $passwordClean = filter_input(INPUT_POST, "password", FILTER_SANITIZE_STRING);
    if ($passwordClean) {
        $password = $passwordClean;
    } else {
        $password = null;
    }
}

/**
 * Crée un nouveau compte
 *
 * @param string $email email de l'utilisateur
 * @param string $password password de l'utilisateur pas encore hashé
 * @param string $erreur erreur
 * @return void
 */
function createAccount($email, $password, &$erreur)
{
    $myDB = connectDB();

    try {
        $sql = 'INSERT INTO utilisateurs (`email`, `password`) VALUES (:email, :password)';
        $query = $myDB->prepare($sql);
        $query->execute(["email" => $email, "password" => password_hash($password, PASSWORD_BCRYPT)]);
    } catch (Exception $e) {
        // echo $e->getMessage();
        //echo $e->getCode(); -> permet de voir le code d'erreur
        switch ($e->getCode()) {
                //erreur si l'adresse mail est déjà utilisée
            case 23000:
                $erreur = "L'adresse mail est déjà utilisée !";
                break;
        }
    }
}
/**
 * Se connecte à la base
 *
 * @param string $email email de l'utilisateur
 * @param string $password mot de passe
 * @return bool True si la connexion a réussi, sinon retourne false
 */
function connect($email, $password)
{
    $myDB = connectDB();

    try {
        $sql = 'SELECT `password` FROM utilisateurs WHERE `email` LIKE :email';
        $query = $myDB->prepare($sql);
        $query->execute(["email" => $email]);
        $result = $query->fetch(PDO::FETCH_ASSOC);
        if (count($result) == 1) {

            if (password_verify($password, $result["password"])) {
                return true;
            } else {
                return false;
            }
        }
    } catch (Exception $e) {

        $e->getMessage();
        return false;
    }
}

/**
 * Fonction qui retourne un la valeur d'un string avant un certain caractère
 *
 * @param [type] $t
 * @param [type] $i
 * @return string
 */
function before($t, $i)
{
    return substr($i, 0, strpos($i, $t));
};

/**
 * Retourne tous les vehicules de la base
 *
 * @return Object ou Boolean un tableau si la requete a marché, sinon false   
 */
function getVehicles()
{
    $myDB = connectDB();

    try {
        $sql = 'SELECT * FROM vehicules';
        $query = $myDB->prepare($sql);
        $query->execute();

        return $query;
    } catch (PDOException $e) {
        return false;
    }
    return false;
}

/**
 * Get today
 *
 * @return string date as a string "YYYY-mm-dd"
 */
function getToday() {
    return date("Y-m-d");
}

/**
 * Get the first day of this week
 *
 * @return string date as a string "YYYY-mm-dd"
 */
function getThisWeek() {
    $d = new DateTime();
    return date_format($d->setISODate(date("Y"), date("W"), 1), "Y-m-d");;
}

/**
 * Get the first day of this month
 *
 * @return string date as a string "YYYY-mm-dd"
 */
function getThisMonth() {
    return date("Y-m-") . "01";
}

/**
 * Get the first day of this year
 *
 * @return string  date as a string "YYYY-mm-dd"
 */
function getThisYear() {
    return date("Y-") . "01-01";
}