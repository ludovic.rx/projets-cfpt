<?php 
// Projet    :   h2lem4.cfpt.info
// Auteur    :   Ludovic Roux,Souza Luz Juliano
// Desc.     :   Constantes du projet h2lem4.cfpt.info
// Version   :   1.0, 09.09.2020, LR et JSL, version initiale

// Constantes de la base de donnée
define("DB_NAME", "h2lem");// Nom de la base de donnée
define("DB_USER", "developpeur");// Nom du user de la base de donnée
define("DB_PASS", "Hambrek");// Mot de passe du user
define("DB_IP", "127.0.0.1");// Adresse ip de la base de donnée

// Constantes de messages
define("ERREUR_AJOUT", "Ajout non-réussi");// Mesage d'erreur lors de l'ajout
define("PAS_ERREUR_AJOUT", "Ajout réussi");// Message de réussite lors de l'ajout
define("ERREUR_RECHERCHE", "Veuillez remplir le champ de recherche");// Message d'erreur si on ne remplit pas le champ nom lors de la recherche

// Constantes style
define("CLASSE_ERREUR", "erreur");// Classe des champs qui affichent une erreur
define("CLASSE_PAS_ERREUR", "pas_erreur");// Classe des champs qui n'ont pas d'erreur 