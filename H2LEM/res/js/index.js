// Projet    :   h2lem4.cfpt.info
// Auteur    :   Ludovic Roux,Souza Luz Juliano
// Desc.     :   Fonctions de l'index
    
function pressLink(sender) {
    let date = document.getElementById("date");
    sender.href += "&&date=" + date.value;
}

function changeDate(sender, idVehicule) {
    document.getElementById("formSearch").action = "index.php?id=" + idVehicule + "&&date=" + sender.value;
}