// Projet    :   h2lem4.cfpt.info
// Auteur    :   Ludovic Roux,Souza Luz Juliano
// Desc.     :   fonctions pour la map du projet h2lem4.cfpt.info

var mymap = L.map('mapid').setView([51.505, -0.09], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
	maxZoom: 18,
	attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
		'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
		'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
	id: 'mapbox/streets-v11',
	tileSize: 512,
	zoomOffset: -1
}).addTo(mymap);

var popup = L.popup();

// function onMapClick(e) {
// 	popup
// 		.setLatLng(e.latlng)
// 		.setContent("You clicked the map at " + e.latlng.toString())
// 		.openOn(mymap);
// }

// mymap.on('click', onMapClick);


/**
 * Dessine la chemin emprunté par le vélo
 * @param {array} values tableau qui contient la latitude et la longitude de tous les points
 */
function drawTrack(values) {
	let latlngs = [];

	values.forEach(coordonnees => {
		latlngs.push([coordonnees.lati, coordonnees.longi]);
	});

	let polyline = L.polyline(latlngs, {
		color: values[0].color
	}).addTo(mymap);
	mymap.fitBounds(polyline.getBounds());
}

function getDistance(origin, destination) {
	// return distance in meters
	var lon1 = toRadian(origin[1]),
		lat1 = toRadian(origin[0]),
		lon2 = toRadian(destination[1]),
		lat2 = toRadian(destination[0]);

	var deltaLat = lat2 - lat1;
	var deltaLon = lon2 - lon1;

	var a = Math.pow(Math.sin(deltaLat / 2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(deltaLon / 2), 2);
	var c = 2 * Math.asin(Math.sqrt(a));
	var EARTH_RADIUS = 6371;
	return c * EARTH_RADIUS * 1000;
}

function toRadian(degree) {
	return degree * Math.PI / 180;
}

function distanceEntrePoints(tab) {
	let distance = 0;
	for (let c = 0; c < tab.length; c++) {
		if (c > 0) {
			distance += getDistance([tab[c - 1].lati, tab[c - 1].longi], [tab[c].lati, tab[c].longi]);
		}
	}
	return distance;
}

function calculerVitesse(tab) {
	let lastValues = tab.reverse().slice(0, 5); // Five last values
	temp = (distanceEntrePoints(lastValues) / ((new Date(Date.parse(lastValues[0].time)) - new Date(Date.parse(lastValues[lastValues.length - 1].time))) / 1000)) * 3.6;
	temp *= 100;
	temp = Math.round(temp);
	temp = temp / 100;
	return temp;
}

if (varJson) {

	if (all == true) {
		if(varJson)
		varJson.forEach(element => {
			if (element.length > 0) {
				drawTrack(element);
			}
		})
	} else {
		document.getElementById("distance").innerHTML += " : " + (Math.round(distanceEntrePoints(varJson) / 1000)) + " km";
		document.getElementById("vitesse").innerHTML += " : " + calculerVitesse(varJson) + " km/h";
		drawTrack(varJson);
	}

}