<!DOCTYPE html>
<?php
// Projet    :   Search in book
// Auteur    :   Ludovic Roux
// Desc.     :   Page search qui affiche le résultat d'une recherche
// Version   :   1.0, 24.11.2020, LR, version initiale

require_once("res/php/functions.inc.php");
require_once("res/php/session.inc.php");

define("VALUE_BUTTON_FIRST", "First");
define("VALUE_BUTTON_PREVIOUS", "Previous");
define("VALUE_BUTTON_NEXT", "Next");
define("VALUE_BUTTON_LAST", "Last");
define("NAME_BUTTON_SUBMIT", "btnSubmit");
define("LIMIT", 15);

$offset = $_SESSION["offset"];
$limit = LIMIT;
$size = 0;

$search = "";
$typeReasearch = 0;
$submit = filter_input(INPUT_POST, "btnSubmit", FILTER_SANITIZE_STRING);

$result = array();

if ($submit == "Rechercher") {
    $search = filter_input(INPUT_POST, "search", FILTER_SANITIZE_STRING);
    $typeReasearch = filter_input(INPUT_POST, "typeReasearch", FILTER_SANITIZE_STRING);

    if (strlen($search) != 0) {
        switch ($typeReasearch) {
            case -1:
            case 0:
                // Search in natural language mode
                $result = searchInBooksNaturalLanguage($search, $limit, $offset);
                break;
            case 1:
                // Search in natural langauge mode with query expansion
                $result = searchInBooksNaturalLanguageQueryExpansion($search);
                break;
            case 2:
                // Search with query expansion
                $result = searchInBooksQueryExpansion($search);
                break;
            case 3:
                $result = searchLike($search);
            break;
        }
        $size = count($result);
    }
}

?>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="./res/css/main.css">

    <title>Books</title>
</head>

<body>
    <?php include("./res/php/header.inc.php"); ?>
    <main>
        <?= showResults($result, $search) ?>
    </main>
    <footer class="footer mt-auto py-3 bg-light sticky-bottom">
        <div class="container">
            <nav aria-label="Page navigation example">
                <form method="POST" action="#">
                    <ul class="pagination d-flex justify-content-center m-auto">
                        <li class="page-item"><input class="page-link" type="submit" name="<?= NAME_BUTTON_SUBMIT ?>" value="<?= VALUE_BUTTON_FIRST ?>"></li>
                        <li class="page-item"><input class="page-link" type="submit" name="<?= NAME_BUTTON_SUBMIT ?>" value="<?= VALUE_BUTTON_PREVIOUS ?>"></li>
                        <li class="page-item mr-2 ml-2  d-flex align-items-center"><span><?= $offset ?> / <?= $size ?></span></li>
                        <li class="page-item"><input class="page-link" type="submit" name="<?= NAME_BUTTON_SUBMIT ?>" value="<?= VALUE_BUTTON_NEXT ?>"></li>
                        <li class="page-item"><input class="page-link" type="submit" name="<?= NAME_BUTTON_SUBMIT ?>" value="<?= VALUE_BUTTON_LAST ?>"></li>
                    </ul>
                </form>
            </nav>
        </div>
    </footer>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>

</html>