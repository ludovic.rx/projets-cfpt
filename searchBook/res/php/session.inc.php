<?php
// Projet    :   Search in book
// Auteur    :   Ludovic Roux
// Desc.     :   Crée la session et set les valeurs par défaut
// Version   :   1.0, 24.11.2020, LR, version initiale

session_start();

if(!isset($_SESSION["offset"])) {
    $_SESSION["offset"] = 0;
}
