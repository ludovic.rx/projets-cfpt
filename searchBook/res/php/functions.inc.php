<?php
// Projet    :   Search in book
// Auteur    :   Ludovic Roux
// Desc.     :   Constantes du projet notes
// Version   :   1.0, 24.11.2020, LR, version initiale

include_once("res/php/database.inc.php");

/**
 * Insert a site
 *
 * @param string $title 
 * @param string $content 
 * @param string $lineNumber
 * @return bool true if succeed else false
 */
function insertBooks($title, $lineNumber, $content, &$codeError)
{
    static $ps = null;
    $sql = "INSERT INTO `bigdatas` (`title`, `linenumber`, `content`) VALUES(:TITLE, :LINE_NUMBER, :CONTENT)";

    $answer = false;
    $codeError = -1;

    try {
        if ($ps == null) {
            $ps = connectDbBook()->prepare($sql);
        }
        $content = utf8_encode($content);

        $ps->bindParam(':TITLE', $title, PDO::PARAM_STR);
        $ps->bindParam(':LINE_NUMBER', $content, PDO::PARAM_STR);
        $ps->bindParam(':CONTENT', $lineNumber, PDO::PARAM_STR);

        $answer = $ps->execute();
    } catch (PDOException $e) {
        echo $e->getMessage();
        $codeError = $e->getCode();
    }

    return $answer;
}

/**
 * Get the contents of an url
 *
 * @param string $book
 * @return string|false content of the site, that is cleared
 */
function getContent($book, &$title)
{
    // Ouvre le fichier via l'url
    $handle = fopen($book, "r");
    // Obtient le contenu du site
    $result = stream_get_contents($handle);
    preg_match("/<title.*>.*<\/title>/", $result, $title);
    $title[0] = strip_tags($title[0]);

    // Enlève tout le script
    $result = preg_replace("/<script ?.*>.*<\/script>/sU", "", $result);
    // Enlève les balises de style
    $result = preg_replace("/<style ?.*>.*<\/style>/sU", "", $result);
    // Enlève toutes les nav
    $result = preg_replace("/<nav ?.*>.*<\/nav>/sU", "", $result);


    // Prends le contenu du body
    preg_match("/<body ?.*>.*<\/body>/is", $result, $result);
    // Enlève les balises HTML
    $result[0] = strip_tags($result[0]);

    // Enlèves les espaces inutiles à l'intérieur de la chaîne
    $result[0] = preg_replace("/\s\s+/", " ", $result[0]);

    if (count($result) == 0) {
        return false;
    }
    return $result[0];
}

/**
 * Search in the books that are in the database with the natural language mode
 *
 * @param string $search search 
 * @param int $limit limit 
 * @param int $offset offset 
 * @return array|false 
 */
function searchInBooksNaturalLanguage($search, $limit, $offset)
{
    static $ps = null;
    $sql = "SELECT title, linenumber, content, ROUND(MATCH (content) AGAINST (:SEARCH IN NATURAL LANGUAGE MODE), 2) AS score 
        FROM bigdatas 
        WHERE MATCH (content) AGAINST (:SEARCH2 IN NATURAL LANGUAGE MODE) > 0
        ORDER BY score DESC
        -- LIMIT :OFFSET, :LIMIT ";

    $answer = false;
    try {
        if ($ps == null) {
            $ps = connectDbBook()->prepare($sql);
        }
        $ps->bindParam(':SEARCH', $search, PDO::PARAM_STR);
        $ps->bindParam(':SEARCH2', $search, PDO::PARAM_STR);
        // $ps->bindParam(':LIMIT', $limit, PDO::PARAM_INT);
        // $ps->bindParam(':OFFSET', $offset, PDO::PARAM_INT);
        $ps->execute();

        $answer = $ps->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        $answer = array();
        echo $e->getMessage();
    }

    return $answer;
}

/**
 * Search in the books that are in the database with the natural language mode with query expansion
 *
 * @param string $search search 
 * @return array|false 
 */
function searchInBooksNaturalLanguageQueryExpansion($search)
{
    static $ps = null;
    $sql = "SELECT title, linenumber, content, ROUND(MATCH (content) AGAINST (:SEARCH IN NATURAL LANGUAGE MODE WITH QUERY EXPANSION), 2) AS score
        FROM bigdatas
        WHERE MATCH (content) AGAINST (:SEARCH2 IN NATURAL LANGUAGE MODE WITH QUERY EXPANSION) > 30
        ORDER BY score DESC";

    $answer = false;
    try {
        if ($ps == null) {
            $ps = connectDbBook()->prepare($sql);
        }
        $ps->bindParam(':SEARCH', $search, PDO::PARAM_STR);
        $ps->bindParam(':SEARCH2', $search, PDO::PARAM_STR);
        $ps->execute();

        $answer = $ps->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        $answer = array();
        echo $e->getMessage();
    }

    return $answer;
}

/**
 * Search in the books that are in the database
 *
 * @param string $search search 
 * @return array|false 
 */
function searchInBooksQueryExpansion($search)
{
    static $ps = null;
    $sql = "SELECT title, linenumber, content, ROUND(MATCH (content) AGAINST (:SEARCH WITH QUERY EXPANSION), 2) AS score
        FROM bigdatas
        WHERE MATCH (content) AGAINST (:SEARCH2 WITH QUERY EXPANSION) > 30
        ORDER BY score DESC";

    $answer = false;
    try {
        if ($ps == null) {
            $ps = connectDbBook()->prepare($sql);
        }
        $ps->bindParam(':SEARCH', $search, PDO::PARAM_STR);
        $ps->bindParam(':SEARCH2', $search, PDO::PARAM_STR);
        $ps->execute();

        $answer = $ps->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        $answer = array();
        echo $e->getMessage();
    }

    return $answer;
}

/**
 * Search with like
 *
 * @param string $search
 * @return array|false
 */
function searchLike($search)
{
    static $ps = null;
    $sql = "SELECT title, linenumber, content
        FROM bigdatas
        WHERE content LIKE :CONTENT";

    $answer = false;
    try {
        if ($ps == null) {
            $ps = connectDbBook()->prepare($sql);
        }

        $search = "%$search%";
        $ps->bindParam(':CONTENT', $search, PDO::PARAM_STR);
        $ps->execute();

        $answer = $ps->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        $answer = array();
        echo $e->getMessage();
    }

    return $answer;
}

/**
 * Ecrit le résultat de la recherche
 *
 * @param array $result result of the research
 * @return string table in html or a error message
 */
function showResults($result, $search)
{
    $answer = "";
    $words = explode(" ", $search);
    $colors = createColor(count($words));

    if (count($result) == 0) {
        $answer = "Aucun résultat n'a été trouvé.";
    } else {
        $hasScore = isset($result[0]["score"]);
        $answer = '<table class="table table-hover text-justify">';
        $answer .= "<thead class='text-center'>";
        $answer .= "<tr>";
        $answer .= "<th>Titre</th>";
        $answer .= "<th>Numéro de ligne</th>";
        $answer .= "<th>Contenu</th>";
        $answer .= ($hasScore) ? "<th>Score</th>" : "";
        $answer .= "</tr>";
        $answer .= "</thead>";
        $answer .= "<tbody>";
        foreach ($result as $line) {
            $answer .= "<tr>";
            $answer .= '<td>' . $line["title"] . '</td>';
            $answer .= '<td>' . $line["linenumber"] . '</td>';
            $answer .= "<td>" .   highlightWord($line["content"], $colors, $words) . "</td>";
            // $answer .= "<td>" .  $line["content"] . "</td>";
            $answer .= ($hasScore) ? "<td>" .  $line["score"] . "</td>" : "";
            $answer .= "</tr>";
        }
        $answer .= "</tbody>";
        $answer .= "</table>";
    }

    return $answer;
}

/**
 * Higlight the words that correspond to the research
 *
 * @param array $content
 * @param array $colors
 * @param array $words
 * @return array
 */
function highlightWord($content, $colors, $words)
{
    for ($numWord = 0; $numWord < count($words); $numWord++) {
        $word =  $words[$numWord];

        $matches = array();
        preg_match_all("/$word/im", $content, $matches, PREG_OFFSET_CAPTURE);
        for ($i = 0; $i < count($matches[0]); $i++) {
            $position = $matches[0][$i][1];

            $content = substr($content, 0, $position) . "<mark style='background-color:rgba(" . $colors[$numWord][0] . "," . $colors[$numWord][1] . "," . $colors[$numWord][2] . ", 0.5)'>" . substr($content, $position, strlen($word)) . "</mark>" .  substr($content, $position + strlen($word));
            preg_match_all("/$word/im", $content, $matches, PREG_OFFSET_CAPTURE);
        }
    }

    return $content;
}

/**
 * Create the table of colors
 *
 * @param int $numberColor count of colors
 * @return array
 */
function createColor($numberColor)
{
    $result = array();
    for ($i = 0; $i < $numberColor; $i++) {
        array_push($result, array(rand(0, 255), rand(0, 255), rand(0, 255)));
    }
    return $result;
}

/**
 * Write a response to the action
 *
 * @param int $codeError code of error
 * @return string ressponse
 */
function writeResponse($codeError)
{
    $result = "";
    switch ($codeError) {
        case -1:
            $result = "<div class='valid-feedback d-block text-center w-50 m-auto'>L'insertion a fonctionnée.</div>";
            break;
        case 23000:
            $result = "<div class='invalid-feedback d-block text-center w-50 m-auto'>Le site existe déjà dans la base.</div>";
            break;
    }
    return $result;
}

/**
 * Writes the select
 *
 * @return string result
 */
function writeSelect($selected)
{
    $valuesSelect = array("Natural Language Mode", "Natural Language With Query Expansion", "With Query Expansion", "Like");
    $anwser = '<select class="form-control text-success border-success my-2" name="typeReasearch" aria-labelledby="dropdownMenuLink">';
    foreach ($valuesSelect as $key => $value) {
        $anwser .= '<option onclick="repaceText(this)" value="' . $key . '" ' . ($selected == $key ? "selected" : "") . '>' . $value . '</option>';
    }
    $anwser .= "</select>";
    return $anwser;
}


/**
 * Goes to the previous page
 *
 * @param int $offset the current offset
 * @param int $limit size of one page
 * @return int the offset of the previous page
 */
function goToPreviousPage($offset, $limit)
{
    $offset -= $limit;
    if ($offset < 0) {
        $offset = 0;
    }
    return $offset;
}

/**
 * Goes to the next page
 *
 * @param int $offset the current offset
 * @param int $limit size of one page
 * @param int $size number of record
 * @return int
 */
function goToNextPage($offset, $limit, $size)
{
    $offset += $limit;
    if ($offset > $size) {
        $offset = goToLastPage($size, $limit);
    }
    return $offset;
}

/**
 * Go to the last page 
 *
 * @param int $limit size of one page
 * @param int $size number of record
 * @return int the offset of the last page
 */
function goToLastPage($limit, $size)
{
    if ($size % $limit != 0) {
        return $size - ($size % $limit);
    } else {
        return $size - $limit;
    }
}
