<?php
// Projet    :   Search in book
// Auteur    :   Ludovic Roux
// Desc.     :   Fonction qui gère la connection à la base de donnée
// Version   :   1.0, 24.11.2020, LR, version initiale

include_once("constantes.inc.php");

/**
 * Se connecte à la base de donnée
 * Le script meurt (die) si la connexion n'est pas possible.
 * @staticvar PDO $dbc
 * @return PDO base de donnée
 */
function connectDbBook()
{
    static $myDb = null;

    if ($myDb == null) {
        try {
            $myDb = new PDO(
                "mysql:host=" . DB_IP . ";dbname=" . DB_NAME . ";charset=utf8mb4",
                DB_USER,
                DB_PASS,
                [
                    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4",
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_EMULATE_PREPARES => false,
                    PDO::ATTR_PERSISTENT => true // Demande de garder la connection (fait par défaut par le Zend framework)
                ]
            );
        } catch (PDOException $e) {
            echo 'Erreur : ' . $e->getMessage(). '<br/>';
            echo 'N° : ' . $e->getCode();
            // Quitte le script et meurt
           die("Could not connect to MySQL");
        }
    }

    return $myDb;
}
