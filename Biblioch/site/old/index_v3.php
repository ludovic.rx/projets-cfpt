<?php
// Projet    :   Biblio CH
// Auteur    :   Ludovic Roux
// Desc.     :   Permet de rechercher des informtions das une base de donnée, de rajouter et de modifier des enrées dans la base
// Version   :   1.0, 03.09.2020, LR, version initiale
include_once("biblioch_functions.php");
include_once("constantes.php");

$myDb = connectDB();

$auteur = "";
$titre = "";
$annee = "";

$affichage = "";
$messageAjout = "";

if (filter_input(INPUT_POST, 'recherche')) {
    $auteur = filter_input(INPUT_POST, 'auteur', FILTER_SANITIZE_STRING);
    $affichage = search($auteur);

} else if (filter_input(INPUT_POST, 'ajout')) {
    $auteur = filter_input(INPUT_POST, 'auteur', FILTER_SANITIZE_STRING);
    $titre = filter_input(INPUT_POST, 'titre', FILTER_SANITIZE_STRING);
    $annee = filter_input(INPUT_POST, 'annee', FILTER_SANITIZE_STRING);

    if ($auteur && $titre && $annee) {
        $sql = "INSERT INTO livres (auteur, titre, annee ) VALUES (:auteur, :titre, :annee)";
        $query = $myDb->prepare($sql);
        $query->execute([":auteur" =>  $auteur, ":titre" => $titre, ":annee" => $annee]);
        $messageAjout = PAS_ERREUR_AJOUT;
    } else {
        $messageAjout = ERREUR_AJOUT;
    }
}



?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BiblioCh</title>
</head>
<style>
    fieldset {
        border: 1px solid black;
        padding: 1%;
    }
</style>

<body>
    <fieldset>
        <legend>Recherche</legend>
        <form method="POST" action="#">
            <input type="text" placeholder="Entrez le nom d'un écrivain" name="auteur">
            <input type="submit" value="Envoyer" name="recherche">
        </form>
        <?php
        echo $affichage;
        ?>
    </fieldset>
    <fieldset>
        <legend>Ajout</legend>
        <form method="POST" action="#">
            <input type="text" placeholder="Entrez le nom d'un écrivain" name="auteur">
            <input type="text" placeholder="Entrez le titre d'une oeuvre" name="titre">
            <input type="text" placeholder="Entrez l'année de parution'" name="annee">
            <input type="submit" value="Envoyer" name="ajout">
        </form>
        <?= $messageAjout ?>
    </fieldset>
</body>

</html>