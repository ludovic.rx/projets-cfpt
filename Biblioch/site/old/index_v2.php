<?php
include("biblioch_functions.php");

$myDb = connectDB();

$auteur = "";
if (filter_input(INPUT_POST, 'submit')) {
    $auteur = filter_input(INPUT_POST, 'auteur', FILTER_SANITIZE_STRING);
}

$sql = "SELECT * FROM livres WHERE auteur LIKE :auteur";
$query = $myDb->prepare($sql);
$query->execute([":auteur" => "%" . $auteur . "%"]);

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BiblioCh</title>
</head>

<body>
    <form method="POST" action="#">
        <input type="text" placeholder="Entrez le nom d'un écrivain" name="auteur">
        <input type="submit" value="Envoyer" name="submit">
    </form>
    <?php
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    foreach ($result as $key => $record) {
        echo $record["auteur"] . ", " . $record["titre"] . ", " . $record["annee"] . "<br/>";
    }
    ?>
</body>

</html>