<?php
include("biblioch_functions.php");

$myDb = connectDB();

$sql = "SELECT * FROM livres";
$query = $myDb->prepare($sql);
$query->execute();

$auteur = "";

if (filter_input(INPUT_POST, 'submit')) {
    $auteur = filter_input(INPUT_POST, 'auteur', FILTER_SANITIZE_STRING);
}

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BiblioCh</title>
</head>

<body>
    <?php
    $result = $query->fetchAll(PDO::FETCH_ASSOC);

    foreach ($result as $key => $record) {
        echo $record["auteur"] . ", " . $record["titre"] . ", " . $record["annee"] . "<br/>";
    }
    ?>
</body>

</html>