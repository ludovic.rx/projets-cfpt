<?php
// Projet    :   Biblio CH
// Auteur    :   Ludovic Roux
// Desc.     :   Permet de rechercher des informtions das une base de donnée
// Version   :   1.0, 03.09.2020, LR, version initiale
include_once("biblioch_functions.php");
include_once("constantes.php");

$auteur = "";

$affichage = "";
$result = false;

// Si on appuie sur le bouton
if (filter_input(INPUT_POST, 'recherche')) {
    $auteur = filter_input(INPUT_POST, 'auteur', FILTER_SANITIZE_STRING);
    $result = search($auteur);

    if($result != false) {
        $affichage = afficherResultat($result);
    } else {
        $affichage = "<p>".ERREUR_RECHERCHE."</p>";
    }
}

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>BiblioCh</title>
</head>

<body>
    <fieldset>
        <legend>Recherche</legend>
        <form method="POST" action="#">
            <input type="text" placeholder="Entrez le nom d'un écrivain" name="auteur">
            <input type="submit" value="Envoyer" name="recherche">
        </form>
        <?php
            echo $affichage;
        ?>
        <p>
            <a href="index.html">Retour</a>
        </p>
    </fieldset>
</body>

</html>