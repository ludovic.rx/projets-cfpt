// Projet    :   Biblio CH
// Auteur    :   Ludovic Roux
// Desc.     :   Fonctions javascript
// Version   :   1.0, 29.10.2020, LR, version initiale

let divAutoComplete = document.getElementById("auteurAutoComplete");// Div où l'on met les valeurs de l'autocomplete
let inputAuteur = document.getElementById("auteur");// Inpute qui contient les données que l'utilisateur cherche

/**
 * Autocomplete the research that is done in the input to search an author
 * @param {Event} event event that happpens
 */
function searchAutocomplete(event) {
    // Configuration de la requête
    const config = {
        method: "get"
    };

    // Envoie le fetch avec comme parametre en get la value que l'utilisateur cherche
    fetch('res/php/autocomplete.php?name=' + event.target.value, config)
        .then(reponse => { return reponse.json() })
        .then(function (json) {
            let count = 0;
            // remet à zéro la div d'autocompletion
            divAutoComplete.innerHTML = "";

            for (const author of json) {
                count++;

                // Création de la div qui contient le nom de l'auteur
                let div = document.createElement("div");
                div.innerHTML = author.auteur;

                div.classList.add("auteurAutoComplete");
                if (count == json.length) {
                    // Donne un style différent pour la dernière div
                    div.classList.add("last");
                }

                div.addEventListener("click", changeValueInput);

                divAutoComplete.appendChild(div);
            }
        })
}

/**
 * Put the value of the autocomplete on the input text auteur
 */
function changeValueInput() {
    inputAuteur.value = this.innerHTML;
    divAutoComplete.innerHTML = "";
}