<?php
// Projet    :   Biblio CH
// Auteur    :   Ludovic Roux
// Desc.     :   Foctions générales
// Version   :   1.0, 17.09.2020, LR, version initiale

/**
 * Permet de changer de page
 *
 * @param string $nomPage nom de la page de direction
 * @return void
 */
function changerPage($nomPage)
{
    header("Location: " . $nomPage);
    exit();
}
