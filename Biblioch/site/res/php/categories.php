<?php
// Projet    :   Biblio CH
// Auteur    :   Ludovic Roux
// Desc.     :   Fonctions qui permettent de gérer la table categories
// Version   :   1.0, 17.09.2020, LR, version initiale

include_once("database.php");

/**
 * Permet de récupérer les catégories
 *
 * @return array retourne toutes les catégories
 */
function getCategories()
{
    $db = connectDB();

    $sql = "SELECT * FROM categories";
    $query = $db->prepare($sql);
    $query->execute();
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    return $result;
}