<?php
// Projet    :   Biblio CH
// Auteur    :   Ludovic Roux
// Desc.     :   Autocomplet l'input de recherhe par nom d'auteur
// Version   :   1.0, 29.10.2020, LR, version initiale

include_once("livres.php");

$name = filter_input(INPUT_GET, "name", FILTER_SANITIZE_STRING);
$result = array();

if(strlen($name) != 0) {
    $result = selectAuteurs($name);
    if($result != array()) {
        echo json_encode($result);
    }
} else {
    echo "[]";
}