<?php
// Projet    :   Biblio CH
// Auteur    :   Ludovic Roux
// Desc.     :   Fonctions qui permettent de gérer l'affichage
// Version   :   1.0, 17.09.2020, LR, version initiale

/**
 * Affiche le résultat d'une requête select
 *
 * @param Array $array resultat de la requête select
 * @return void
 */
function afficherSelect($array)
{
    $style = 0;
    if ($array) {
        echo "<table>";
        echo "<thead><tr><th>Titre</th><th>Auteur</th><th>Actions</th></tr></thead>";
        echo "<tbody>";

        foreach ($array as $record) {
            echo "<tr class='line" . $style . "'>";
            echo "<td>" . $record["titre"] . "</td><td>" . $record["auteur"] . "</td>";
            echo "<td><div class='actions'><a href='modifier.php?id=" . $record["id"] . "'>M </a><a href='effacer.php?id=" . $record["id"] . "'>D</a></div></td>";
            echo "</tr>";

            $style++;
            $style %= 2;
        }

        echo "</tbody>";
        echo "</table>";
    }
}

/**
 * Afficher les options du select categories
 *
 * @param Array $array tableau qui conteient les options à affichers
 * @param int $idCategorie option qui a été select
 * @return void
 */
function afficherOptionCategories($array, $idCategorie)
{
    foreach ($array as $record) {
        echo "<option value=" . $record["idCategorie"] . " " . ($idCategorie == $record["idCategorie"] ? 'selected' : "") . ">" . $record["nom"] . "</option>";
    }
}

/**
 * Affiche une confirmation de suppression
 *
 * @param Array $array tableau qui contient le livre sélectionné
 * @return void
 */
function confirmationSuppression($array)
{
    echo "Voulez-vous vraiment supprimer <span class='italic white'>" . $array["titre"] . "</span> de <span class='italic white'>" . $array["auteur"] . "</span> ?";
}
