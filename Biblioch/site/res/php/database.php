<?php
// Projet    :   Biblio CH
// Auteur    :   Ludovic Roux
// Desc.     :   Fonction qui gère la connection à la base de donnée
// Version   :   1.0, 17.09.2020, LR, version initiale

include_once("constantes.php");

/**
 * Se connecte à la base de donnée
 *
 * @return PDO base de donnée
 */
function connectDB()
{
    static $myDb = null;

    try {
        if ($myDb === null) {
            $myDb = new PDO(
                "mysql:host=" . DB_IP . ";dbname=" . DB_NAME . ";charset=utf8",
                DB_USER,
                DB_PASS,
                [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_EMULATE_PREPARES => false
                ]
            );
        }
    } catch (PDOException $e) {
        return false;
    }

    return $myDb;
}
