<?php 
// Projet    :   Biblio CH
// Auteur    :   Ludovic Roux
// Desc.     :   Constantes du projet biblioch
// Version   :   1.0, 03.09.2020, LR, version initiale

// Constantes de la base de donnée
define("DB_NAME", "biblioch");// Nom de la base de donnée
define("DB_USER", "biblioch");// Nom du user de la base de donnée
define("DB_PASS", "Super");// Mot de passe du user
define("DB_IP", "127.0.0.1");// Adresse ip de la base de donnée

// Constantes de messages
define("ERREUR_AJOUT", "Ajout non-réussi");// Mesage d'erreur lors de l'ajout
define("PAS_ERREUR_AJOUT", "Ajout réussi");// Message de réussite lors de l'ajout
define("ERREUR_RECHERCHE", "Veuillez remplir le champ de recherche");// Message d'erreur si on ne remplit pas le champ nom lors de la recherche
define("PAS_ERREEUR_MODIFICATION", "La modification a réussi");// Message de réussite lors de la modification
define("ERREUR_MODIFICATION", "La modification n'a pas réussi");// Mesage d'erreur lors de la modification

// Constantes style
define("CLASSE_ERREUR", "erreur");// Classe des champs qui affichent une erreur
define("CLASSE_PAS_ERREUR", "pas_erreur");// Classe des champs qui n'ont pas d'erreur 

// Constantes des pages
define("PAGE_ACCEUIL", "index.php");