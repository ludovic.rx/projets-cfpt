<?php
// Projet    :   Biblio CH
// Auteur    :   Ludovic Roux
// Desc.     :   Fonction qui gère le CRUD sur la base livres
// Version   :   2.0, 29.10.2020, LR, version initiale

include_once("database.php");

/**
 * Permet de rechercher des livres selon l'auteur
 *
 * @param string $name nom de l'auteur
 * @return Array ou Boolean, false si la requête n'a pas marché, sinon retourne l'array
 */
function selectLivres($name)
{
    $db = connectDB();

    if ($name) {
        $sql = "SELECT * FROM livres WHERE auteur LIKE :auteur ORDER BY auteur";
        $query = $db->prepare($sql);
        $query->execute([":auteur" => "%" . $name . "%"]);
        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

    return false;
}

/**
 * Sélectionne un livre par rapport à son id
 *
 * @param int $id id du livre
 * @return Array ou Boolean, retourn un array si la requête a réussi, sinon retourne false
 */
function selectLivreById($id)
{
    $db = connectDB();

    if ($id) {
        $sql = "SELECT * FROM livres WHERE id LIKE :id";
        $query = $db->prepare($sql);
        $query->execute([":id" => $id]);
        $result = $query->fetch(PDO::FETCH_ASSOC);

        return $result;
    }

    return false;
}

/**
 * Permet d'afficher une entrée dans la base de donnée
 *
 * @param string $auteur nom de l'auteur du livre
 * @param string $titre nom du livre
 * @param int $annee année de parution du livre
 * @param int $idCategorie catégorie du livre qui est un entier
 * @return bool indique si la requete a fonctionnée
 */
function addLivres($auteur, $titre, $annee, $idCategorie)
{
    $db = connectDB();

    try {
        if ($auteur && $titre && $annee && $idCategorie) {
            $sql = "INSERT INTO livres (auteur, titre, annee, idCategorie) VALUES (:auteur, :titre, :annee, :idCategorie)";
            $query = $db->prepare($sql);
            $query->execute([":auteur" =>  $auteur, ":titre" => $titre, ":annee" => $annee, ":idCategorie" => $idCategorie]);

            return true;
        } else {
            return false;
        }
    } catch (Exception $e) {
        return false;
    }
}

/**
 * Supprime un livre de la table en fonction de son id 
 *
 * @param int $id id du livre que l'on veut supprimer
 * @return Boolean retourn true si la suppression a réussi, sinon restourne false
 */
function deleteLivres($id)
{
    $db = connectDb();

    try {
        if ($id) {
            $sql = "DELETE FROM `livres` WHERE id = :id";
            $query = $db->prepare($sql);
            $query->execute(["id" =>  $id]);

            return true;
        }
        return false;
    } catch (Exception $e) {
        return false;
    }
}

/**
 * Permet d'update un livre
 *
 * @param int $id id du livre
 * @param string $auteur nom de l'auteur
 * @param string $titre titre du livre
 * @param int $annee année de parution
 * @param int $idCategorie id de la catégorie du livre
 * @return Boolean retourn true si l'update a fonctionné, sinon retourne false
 */
function updateLivre($id, $auteur, $titre, $annee, $idCategorie)
{
    $db = connectDB();

    try {
        if ($id && $auteur && $titre && $annee && $idCategorie) {
            $sql = "UPDATE `livres` SET `auteur`= :auteur,`titre`= :titre,`annee`= :annee,`idCategorie` = :idCategorie WHERE id = :id";
            $query = $db->prepare($sql);
            $query->execute(["id" => $id, "auteur" => $auteur, "titre" => $titre, "annee" => $annee, "idCategorie" => $idCategorie]);

            return true;
        } else {
            return false;
        }
    } catch (Exception $e) {
        return false;
    }
}

/**
 * Select the authors by the name in the db
 *
 * @param string $name name researched
 * @return array|false array if succeed, else false
 */
function selectAuteurs($name)
{
    static $ps = null;
    $sql = "SELECT DISTINCT `auteur` FROM `livres` WHERE `auteur` LIKE :auteur";

    try {
        if ($ps == null) {
            $ps = connectDB()->prepare($sql);
        }

        $name = "%$name%";

        $ps->bindParam(":auteur", $name, PDO::PARAM_STR);
        $ps->execute();
        return $ps->fetchAll(PDO::FETCH_ASSOC);
    } catch (Exception $e) {
        echo $e->getMessage();
        return false;
    }
}
