<?php
// Projet    :   Biblio CH
// Auteur    :   Ludovic Roux
// Desc.     :   permet de rechercher un livre dans la base de donnée
// Version   :   2.0, 29.10.2020, LR
include_once("res/php/affichage.php");
include_once("res/php/livres.php");

$auteur = "";
$auteur = filter_input(INPUT_POST, "auteur", FILTER_SANITIZE_STRING);

// Permet d'afficher toutes les entrées si on a rien entré
if (strlen($auteur) == 0) {
    $auteur = "%";
}

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="res/css/style.css" rel="stylesheet">
    <title>BiblioCh</title>
</head>

<body>
    <main>
        <fieldset>
            <legend>Recherche</legend>
            <form method="POST" action="#">
                <label for="auteur" class="filtrer">Filtrer : </label>
                <div class="auteur">
                    <input type="text" placeholder="Entrez le nom d'un écrivain" name="auteur" id="auteur" onkeyup="searchAutocomplete(event)">
                   <div id="auteurAutoComplete" class="containerAutocomplete">

                   </div>
                </div>
                <input type="submit" value="Envoyer" name="recherche" class="envoyer">
            </form>
            <div class="containerTable">
                <?php
                afficherSelect(selectLivres($auteur));
                ?>
            </div>
            <button onclick="window.location.href='ajouter.php'">Ajouter</button>
        </fieldset>
    </main>
    <script type="text/javascript" src="res/js/main.js"></script>
</body>

</html>