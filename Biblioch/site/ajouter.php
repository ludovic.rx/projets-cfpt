<?php
// Projet    :   Biblio CH
// Auteur    :   Ludovic Roux
// Desc.     :   Permet d'ajouter une entrée dans la base
// Version   :   1.0, 17.09.2020, LR, version initiale
include_once("res/php/constantes.php");
include_once("res/php/livres.php");
include_once("res/php/categories.php");
include_once("res/php/affichage.php");

// Variables qui contiennent la valeur des champs
$auteur = "";
$titre = "";
$annee = "";
$idCategorie = "";

// Contient le style des champs
$styleAuteur = CLASSE_PAS_ERREUR;
$styleTitre = CLASSE_PAS_ERREUR;
$styleAnnee = CLASSE_PAS_ERREUR;
$styleCategorie = CLASSE_PAS_ERREUR;
$styleAjout = "";

// Mssage indiquant si l'ajout a  réussi
$messageAjout = "";

if (filter_input(INPUT_POST, 'ajout')) {
    $auteur = filter_input(INPUT_POST, 'auteur', FILTER_SANITIZE_STRING);
    $titre = filter_input(INPUT_POST, 'titre', FILTER_SANITIZE_STRING);
    $annee = filter_input(INPUT_POST, 'annee', FILTER_SANITIZE_STRING, FILTER_VALIDATE_INT);
    $idCategorie = filter_input(INPUT_POST, "categorie", FILTER_SANITIZE_NUMBER_INT, FILTER_VALIDATE_INT);

    if (addLivres($auteur, $titre, $annee, $idCategorie)) {
        $messageAjout = PAS_ERREUR_AJOUT;
        $auteur = "";
        $titre = "";
        $annee = "";
        $idCategorie = "";
    } else {
        $messageAjout = ERREUR_AJOUT;
        if (!$auteur) {
            $styleAuteur = CLASSE_ERREUR;
        }
        if (!$titre) {
            $styleTitre = CLASSE_ERREUR;
        }
        if (!$annee) {
            $styleAnnee = CLASSE_ERREUR;
        }
        if (!$idCategorie) {
            $styleCategorie = CLASSE_ERREUR;
        }
    }
}

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="res/css/style.css">
    <title>BiblioCh</title>
</head>

<body>
    <main>
        <fieldset>
            <legend>Ajout</legend>
            <form method="POST" action="#" class="ajoutModif">
                <div>
                    <span>
                        <?= $messageAjout ?>
                    </span>
                </div>
                <div class="containerForm">
                    <p>
                        <label for="auteur">Nom de l'écrivain</label>
                        <input type="text" value="<?= $auteur ?>" class="<?= $styleAuteur ?>" placeholder="Entrez le nom d'un écrivain" name="auteur" id="auteur">
                    </p>
                    <p>
                        <label for="titre">Titre de l'oeuvre</label>
                        <input type="text" value="<?= $titre ?>" class="<?= $styleTitre ?>" placeholder="Entrez le titre d'une oeuvre" name="titre" id="titre">
                    </p>
                    <p>
                        <label for="annee">Année de parution</label>
                        <input type="number" value="<?= $annee ?>" class="<?= $styleAnnee ?>" placeholder="Entrez l'année de parution" name="annee" id="annee">
                    </p>
                    <p>
                        <label for="categorie">Type de livre</label>
                        <select name="categorie" id="categorie" class="<?= $styleCategorie ?>">
                            <?php
                            afficherOptionCategories(getCategories(), $idCategorie);
                            ?>
                        </select>
                    </p>

                    <p>
                        <input type="reset" value="Annuler" name="reset">
                        <input type="submit" value="Ajouter" name="ajout">
                    </p>
                </div>

            </form>
            <p class="retour">
                <a href="index.php">Retour</a>
            </p>
        </fieldset>
    </main>
</body>

</html>