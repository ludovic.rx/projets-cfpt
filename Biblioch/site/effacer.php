<?php
// Projet    :   Biblio CH
// Auteur    :   Ludovic Roux
// Desc.     :   Permet d'effacer un livre choisi
// Version   :   1.0, 17.09.2020, LR, version initiale
include_once("res/php/constantes.php");
include_once("res/php/livres.php");
include_once("res/php/affichage.php");
include_once("res/php/functions.php");

$idDelete = filter_input(INPUT_GET, "id", FILTER_SANITIZE_NUMBER_INT, FILTER_VALIDATE_INT);

if (filter_input(INPUT_POST, "supprimer")) {

    if ($idDelete) {
        deleteLivres($idDelete);
        changerPage(PAGE_ACCEUIL);
    }
} else if (filter_input(INPUT_POST, "annuler")) {
    changerPage(PAGE_ACCEUIL);
}

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="res/css/style.css">
    <title>BiblioCh</title>
</head>

<body>
    <main>
        <fieldset class="effacer">
            <legend>Effacer</legend>
            <p>
                <?php
                if ($idDelete != null) {
                    confirmationSuppression(selectLivreById($idDelete));
                }
                ?>
            </p>
            <form action="#" method="POST">
                <input type="submit" value="Annuler" name="annuler">
                <input type="submit" value="Supprimer" name="supprimer">
            </form>
        </fieldset>

    </main>

</body>

</html>