-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 24 sep. 2020 à 14:41
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `biblioch`
--
CREATE DATABASE IF NOT EXISTS `biblioch` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `biblioch`;

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `idCategorie` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`idCategorie`),
  UNIQUE KEY `idCategorie` (`idCategorie`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`idCategorie`, `nom`) VALUES
(1, 'Science-Fiction'),
(2, 'Roman policier'),
(3, 'Scientifique'),
(4, 'Récit'),
(5, 'Roman'),
(6, 'Fantastique');

-- --------------------------------------------------------

--
-- Structure de la table `livres`
--

DROP TABLE IF EXISTS `livres`;
CREATE TABLE IF NOT EXISTS `livres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auteur` varchar(50) DEFAULT NULL,
  `titre` varchar(150) NOT NULL,
  `annee` int(2) DEFAULT NULL,
  `idCategorie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idCategorie` (`idCategorie`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `livres`
--

INSERT INTO `livres` (`id`, `auteur`, `titre`, `annee`, `idCategorie`) VALUES
(1, 'Albert Camus', 'La peste', 1947, 4),
(2, 'Albert Camus', 'L\'étranger', 1942, 5),
(3, 'J.R.R. Tolkien', 'Le seigneur des anneaux I,   La Fraternité de l\'Anneau', 1954, 6),
(4, 'J.R.R. Tolkien', 'Le seigneur des anneaux II,  Les Deux Tours', 1954, 6),
(5, 'J.R.R. Tolkien', 'Le seigneur des anneaux III, Le Retour du Roi', 1955, 6),
(6, 'J.R. Dos Santos', 'Codex 632, le secret de Christophe Colomb', 2005, 6),
(7, 'Camilla Läckberg', 'La Sorcière', 2017, 2),
(8, 'Ernest Cline', 'Player One', 2011, 3),
(9, 'Ernest Cline', 'Armada', 2015, 3),
(10, 'George Orwell', '1984', 1950, 2),
(11, 'Jean-Pierre Luminet', 'L&#39;oeil de Galilée', 2020, 3),
(12, 'J.K. Rowling', 'Harry Potter à l&#39;école des sorciers', 1997, 6),
(13, 'J.K. Rowling', 'Harry Potter à l&#39;école des sorciers', 1997, 6),
(14, 'J.K. Rowling', 'Harry Potter à l&#39;école des sorciers', 1997, 6),
(15, 'J.K. Rowling', 'Harry Potter à l&#39;école des sorciers', 1998, 6);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `livres`
--
ALTER TABLE `livres`
  ADD CONSTRAINT `livres_ibfk_1` FOREIGN KEY (`idCategorie`) REFERENCES `categories` (`idCategorie`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
